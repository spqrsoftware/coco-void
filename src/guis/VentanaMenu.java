package guis;

import clases.*;
import clases.otros.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.TreeMap;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import utilerias.sonido.*;

public class VentanaMenu extends javax.swing.JFrame {
    
    /** Creates new form VentanaMenu */
    public VentanaMenu() {
        initComponents();
        pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        panel_menu.setLocation( pantalla.width/2 - panel_menu.getSize().width/2,
                pantalla.height/2 - panel_menu.getSize().height/2 );
        panel_seleccion.setLocation( pantalla.width/2 - panel_seleccion.getSize().width/2,
                pantalla.height/2 - panel_seleccion.getSize().height/2 );
        panel_cocos.setLocation( pantalla.width/2 - panel_cocos.getSize().width/2,
                pantalla.height/2 - panel_cocos.getSize().height/2 );
        panel_cartas.setLocation( pantalla.width/2 - panel_cartas.getSize().width/2,
                pantalla.height/2 - panel_cartas.getSize().height/2 );
        label_listos.setVisible( false );
        panel_cocos.setVisible( false );
        panel_cartas.setVisible( false );
        centinela = 1;
        contador_cocos = 1;
        jLabel5.setLocation(pantalla.width-jLabel5.getSize().width, jLabel5.getLocation().y);
        MueveFondo mf = new MueveFondo();
        mf.setDaemon( true );
        mf.start();
        panel_seleccion.setVisible( false );
    }   //fin del constructor
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        capa_fondo = new javax.swing.JLayeredPane();
        panel_menu = new javax.swing.JPanel();
        label_salir = new javax.swing.JLabel();
        label_spqr = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        label_jugar = new javax.swing.JLabel();
        panel_seleccion = new javax.swing.JPanel();
        label_jugador = new javax.swing.JLabel();
        label_volver = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_jugador1 = new javax.swing.JTextField();
        label_aceptar = new javax.swing.JLabel();
        label_jugador1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        label_aceptar1 = new javax.swing.JLabel();
        txt_jugador2 = new javax.swing.JTextField();
        label_listos = new javax.swing.JLabel();
        panel_cocos = new javax.swing.JPanel();
        label_jugador_actual = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        label_coco = new javax.swing.JLabel();
        btn_aceptar = new javax.swing.JButton();
        txt = new javax.swing.JLabel();
        txt_nombre_coco = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_atributo_coco = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_bio = new javax.swing.JScrollPane();
        txt_bio_coco = new javax.swing.JTextPane();
        btn_atras = new javax.swing.JButton();
        btn_sig = new javax.swing.JButton();
        panel_cartas = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        box_cantidad_cartas = new javax.swing.JComboBox();
        btn_cargar_todo = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        label_fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Coco Void");
        setResizable(false);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        capa_fondo.setBackground(new java.awt.Color(0, 0, 153));
        capa_fondo.setOpaque(true);
        panel_menu.setBackground(new java.awt.Color(102, 102, 102));
        panel_menu.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        panel_menu.setPreferredSize(new java.awt.Dimension(640, 480));
        label_salir.setFont(new java.awt.Font("Trebuchet MS", 1, 36));
        label_salir.setForeground(new java.awt.Color(255, 255, 255));
        label_salir.setText("Salir");
        label_salir.setToolTipText("");
        label_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_salirMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_salirMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_salirMouseExited(evt);
            }
        });

        label_spqr.setFont(new java.awt.Font("Trebuchet MS", 1, 36));
        label_spqr.setForeground(new java.awt.Color(255, 255, 255));
        label_spqr.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_spqr.setText("SPQR Software");
        label_spqr.setToolTipText("");
        label_spqr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_spqrMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_spqrMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_spqrMouseExited(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo.jpg")));

        label_jugar.setFont(new java.awt.Font("Trebuchet MS", 1, 36));
        label_jugar.setForeground(new java.awt.Color(255, 255, 255));
        label_jugar.setText("Jugar");
        label_jugar.setToolTipText("");
        label_jugar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_jugarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_jugarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_jugarMouseExited(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panel_menuLayout = new org.jdesktop.layout.GroupLayout(panel_menu);
        panel_menu.setLayout(panel_menuLayout);
        panel_menuLayout.setHorizontalGroup(
            panel_menuLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_menuLayout.createSequentialGroup()
                .add(281, 281, 281)
                .add(label_salir)
                .addContainerGap(279, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_menuLayout.createSequentialGroup()
                .addContainerGap(217, Short.MAX_VALUE)
                .add(jLabel1)
                .add(217, 217, 217))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_menuLayout.createSequentialGroup()
                .addContainerGap(280, Short.MAX_VALUE)
                .add(label_jugar)
                .add(262, 262, 262))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_menuLayout.createSequentialGroup()
                .addContainerGap(189, Short.MAX_VALUE)
                .add(label_spqr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 284, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(161, 161, 161))
        );
        panel_menuLayout.setVerticalGroup(
            panel_menuLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_menuLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1)
                .add(27, 27, 27)
                .add(label_jugar)
                .add(27, 27, 27)
                .add(label_spqr)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 33, Short.MAX_VALUE)
                .add(label_salir)
                .add(73, 73, 73))
        );
        panel_menu.setBounds(80, 60, 640, 480);
        capa_fondo.add(panel_menu, javax.swing.JLayeredPane.MODAL_LAYER);

        panel_seleccion.setBackground(new java.awt.Color(102, 102, 102));
        panel_seleccion.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        label_jugador.setFont(new java.awt.Font("Dialog", 1, 18));
        label_jugador.setForeground(new java.awt.Color(255, 255, 255));
        label_jugador.setText("Jugador 1");

        label_volver.setFont(new java.awt.Font("Dialog", 1, 24));
        label_volver.setForeground(new java.awt.Color(255, 255, 255));
        label_volver.setText("Volver");
        label_volver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_volverMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_volverMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_volverMouseExited(evt);
            }
        });

        jLabel3.setBackground(new java.awt.Color(0, 0, 0));
        jLabel3.setForeground(new java.awt.Color(153, 255, 0));
        jLabel3.setText("Digite el nombre del jugador 1");

        txt_jugador1.setToolTipText("Digite su nombre por favor");
        txt_jugador1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_jugador1KeyTyped(evt);
            }
        });

        label_aceptar.setFont(new java.awt.Font("Dialog", 1, 18));
        label_aceptar.setForeground(new java.awt.Color(255, 255, 255));
        label_aceptar.setText("Aceptar");
        label_aceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_aceptarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_aceptarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_aceptarMouseExited(evt);
            }
        });

        label_jugador1.setFont(new java.awt.Font("Dialog", 1, 18));
        label_jugador1.setForeground(new java.awt.Color(255, 255, 255));
        label_jugador1.setText("Jugador 2");

        jLabel4.setBackground(new java.awt.Color(0, 0, 0));
        jLabel4.setForeground(new java.awt.Color(153, 255, 0));
        jLabel4.setText("Digite el nombre del jugador 2");

        label_aceptar1.setFont(new java.awt.Font("Dialog", 1, 18));
        label_aceptar1.setForeground(new java.awt.Color(255, 255, 255));
        label_aceptar1.setText("Aceptar");
        label_aceptar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_aceptar1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_aceptar1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_aceptar1MouseExited(evt);
            }
        });

        txt_jugador2.setToolTipText("Digite su nombre por favor");
        txt_jugador2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_jugador2KeyTyped(evt);
            }
        });

        label_listos.setFont(new java.awt.Font("Dialog", 1, 14));
        label_listos.setForeground(new java.awt.Color(153, 204, 255));
        label_listos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/SPQR logo.jpg")));
        label_listos.setText("Estamos listos. Click aqui para continuar");
        label_listos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        label_listos.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        label_listos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_listosMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_listosMouseEntered(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panel_seleccionLayout = new org.jdesktop.layout.GroupLayout(panel_seleccion);
        panel_seleccion.setLayout(panel_seleccionLayout);
        panel_seleccionLayout.setHorizontalGroup(
            panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_seleccionLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(panel_seleccionLayout.createSequentialGroup()
                            .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(panel_seleccionLayout.createSequentialGroup()
                                    .add(label_jugador)
                                    .add(484, 484, 484)
                                    .add(label_jugador1))
                                .add(panel_seleccionLayout.createSequentialGroup()
                                    .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, txt_jugador1)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 388, Short.MAX_VALUE)
                                    .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                        .add(txt_jugador2)
                                        .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .add(panel_seleccionLayout.createSequentialGroup()
                                    .add(label_aceptar)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 538, Short.MAX_VALUE)
                                    .add(label_aceptar1)))
                            .addContainerGap())
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_seleccionLayout.createSequentialGroup()
                            .add(label_volver)
                            .add(320, 320, 320)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_seleccionLayout.createSequentialGroup()
                        .add(label_listos)
                        .add(191, 191, 191))))
        );
        panel_seleccionLayout.setVerticalGroup(
            panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_seleccionLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(label_jugador)
                    .add(label_jugador1))
                .add(39, 39, 39)
                .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_jugador1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_jugador2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_seleccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(label_aceptar)
                    .add(label_aceptar1))
                .add(157, 157, 157)
                .add(label_listos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 129, Short.MAX_VALUE)
                .add(label_volver)
                .addContainerGap())
        );
        panel_seleccion.setBounds(70, 50, 700, 660);
        capa_fondo.add(panel_seleccion, javax.swing.JLayeredPane.POPUP_LAYER);

        panel_cocos.setBackground(new java.awt.Color(102, 102, 102));
        panel_cocos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        label_jugador_actual.setFont(new java.awt.Font("Tahoma", 1, 24));
        label_jugador_actual.setForeground(new java.awt.Color(153, 255, 0));
        label_jugador_actual.setText("Jugador");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13));
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Seleccione su coco");

        label_coco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Coco_FREE.gif")));

        btn_aceptar.setBackground(new java.awt.Color(0, 0, 0));
        btn_aceptar.setForeground(new java.awt.Color(255, 255, 255));
        btn_aceptar.setText("Aceptar");
        btn_aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_aceptarActionPerformed(evt);
            }
        });

        txt.setForeground(new java.awt.Color(255, 255, 255));
        txt.setText("Nombre:");

        txt_nombre_coco.setEditable(false);
        txt_nombre_coco.setFocusable(false);

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Especialidad:");

        txt_atributo_coco.setEditable(false);
        txt_atributo_coco.setFocusable(false);

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Bio:");

        txt_bio.setEnabled(false);
        txt_bio.setFocusable(false);
        txt_bio.setRequestFocusEnabled(false);
        txt_bio.setVerifyInputWhenFocusTarget(false);
        txt_bio_coco.setEditable(false);
        txt_bio.setViewportView(txt_bio_coco);

        btn_atras.setBackground(new java.awt.Color(0, 0, 0));
        btn_atras.setForeground(new java.awt.Color(255, 255, 255));
        btn_atras.setText("<");
        btn_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_atrasActionPerformed(evt);
            }
        });

        btn_sig.setBackground(new java.awt.Color(0, 0, 0));
        btn_sig.setForeground(new java.awt.Color(255, 255, 255));
        btn_sig.setText(">");
        btn_sig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sigActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panel_cocosLayout = new org.jdesktop.layout.GroupLayout(panel_cocos);
        panel_cocos.setLayout(panel_cocosLayout);
        panel_cocosLayout.setHorizontalGroup(
            panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_cocosLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(label_jugador_actual)
                    .add(panel_cocosLayout.createSequentialGroup()
                        .add(jLabel2)
                        .add(195, 195, 195)
                        .add(btn_aceptar))
                    .add(panel_cocosLayout.createSequentialGroup()
                        .add(btn_atras)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btn_sig)
                        .add(5, 5, 5)
                        .add(label_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 509, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txt_bio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                            .add(jLabel8)
                            .add(txt)
                            .add(txt_nombre_coco, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                            .add(jLabel7)
                            .add(txt_atributo_coco, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))))
                .addContainerGap())
        );
        panel_cocosLayout.setVerticalGroup(
            panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_cocosLayout.createSequentialGroup()
                .addContainerGap()
                .add(label_jugador_actual)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(btn_aceptar))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panel_cocosLayout.createSequentialGroup()
                        .add(txt)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txt_nombre_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(24, 24, 24)
                        .add(jLabel7)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txt_atributo_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(28, 28, 28)
                        .add(jLabel8)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txt_bio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE))
                    .add(label_coco)
                    .add(panel_cocosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(btn_atras)
                        .add(btn_sig)))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        panel_cocos.setBounds(70, 710, 880, 500);
        capa_fondo.add(panel_cocos, javax.swing.JLayeredPane.POPUP_LAYER);

        panel_cartas.setBackground(new java.awt.Color(102, 102, 102));
        panel_cartas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        panel_cartas.setFocusable(false);
        panel_cartas.setRequestFocusEnabled(false);
        panel_cartas.setVerifyInputWhenFocusTarget(false);
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Seleccione la cantidad de cartas a jugar");

        box_cantidad_cartas.setBackground(new java.awt.Color(0, 0, 0));
        box_cantidad_cartas.setForeground(new java.awt.Color(255, 255, 255));
        box_cantidad_cartas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "12", "24", "36", "48", "60" }));

        btn_cargar_todo.setBackground(new java.awt.Color(0, 0, 0));
        btn_cargar_todo.setForeground(new java.awt.Color(255, 255, 255));
        btn_cargar_todo.setText("Aceptar");
        btn_cargar_todo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cargar_todoActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panel_cartasLayout = new org.jdesktop.layout.GroupLayout(panel_cartas);
        panel_cartas.setLayout(panel_cartasLayout);
        panel_cartasLayout.setHorizontalGroup(
            panel_cartasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_cartasLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel9)
                .addContainerGap(184, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_cartasLayout.createSequentialGroup()
                .addContainerGap(253, Short.MAX_VALUE)
                .add(box_cantidad_cartas, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 121, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_cartasLayout.createSequentialGroup()
                .addContainerGap(303, Short.MAX_VALUE)
                .add(btn_cargar_todo)
                .addContainerGap())
        );
        panel_cartasLayout.setVerticalGroup(
            panel_cartasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_cartasLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel9)
                .add(42, 42, 42)
                .add(box_cantidad_cartas, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 131, Short.MAX_VALUE)
                .add(btn_cargar_todo)
                .addContainerGap())
        );
        panel_cartas.setBounds(950, 50, 390, 260);
        capa_fondo.add(panel_cartas, javax.swing.JLayeredPane.MODAL_LAYER);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Salir de Coco Void");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel5MouseExited(evt);
            }
        });

        jLabel5.setBounds(1085, 20, 140, 17);
        capa_fondo.add(jLabel5, javax.swing.JLayeredPane.DRAG_LAYER);

        label_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/InteriorBulto2.jpg")));
        label_fondo.setBounds(0, 0, 1365, 1024);
        capa_fondo.add(label_fondo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(capa_fondo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1659, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(capa_fondo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1321, Short.MAX_VALUE)
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_jugador2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jugador2KeyTyped
        new AePlayWave( "src/musica/efectos/dado.wav" ).start();
    }//GEN-LAST:event_txt_jugador2KeyTyped

    private void txt_jugador1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jugador1KeyTyped
        new AePlayWave( "src/musica/efectos/dado.wav" ).start();
    }//GEN-LAST:event_txt_jugador1KeyTyped
    
    private void btn_aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptarActionPerformed
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        switch( centinela ){
            case 1: GestorJugadores.getInstancia().jugadorActual().setCoco( buscarCoco() );
            GestorJugadores.getInstancia().cambiarTurno();
            centinela++;
            label_jugador_actual.setText( GestorJugadores.getInstancia().jugadorActual().getNombre() );
            break;
            case 2: GestorJugadores.getInstancia().jugadorActual().setCoco( buscarCoco() );
            GestorJugadores.getInstancia().cambiarTurno();
            centinela++;
            panel_cocos.setVisible( false );
            musica_coco.stop();
            musica_coco = null;
            pwl.resume();
            panel_cartas.setVisible( true );
            cocos.clear();
            cocos = null;
        }   //fin del switch
    }//GEN-LAST:event_btn_aceptarActionPerformed
    
    private void btn_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_atrasActionPerformed
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        musica_coco.stop();
        disminuirContadorCoco();
        imagen = new ImageIcon(buscarCoco().getImagen());
        label_coco.setIcon( imagen );
        txt_nombre_coco.setText( buscarCoco().getNombre() );
        txt_atributo_coco.setText(buscarCoco().getEspecialidad().getDescripcion() );
        txt_bio_coco.setText( buscarCoco().getDescripcion() );
        musica_coco = new PlayWaveLoop( buscarCoco().getMusica() );
        musica_coco.start();
    }//GEN-LAST:event_btn_atrasActionPerformed
    
    private void btn_sigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sigActionPerformed
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        musica_coco.stop();
        aumentarContadorCoco();
        imagen = new ImageIcon(buscarCoco().getImagen());
        label_coco.setIcon( imagen );
        txt_nombre_coco.setText( buscarCoco().getNombre() );
        txt_atributo_coco.setText(buscarCoco().getEspecialidad().getDescripcion() );
        txt_bio_coco.setText( buscarCoco().getDescripcion() );
        musica_coco = new PlayWaveLoop( buscarCoco().getMusica() );
        musica_coco.start();
    }//GEN-LAST:event_btn_sigActionPerformed
    
    private void btn_cargar_todoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cargar_todoActionPerformed
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        //Llamar al gestor
        GestorCartas.getInstancia().crearMazo( analizarCartas() );
        VentanaDados vc = new VentanaDados();
        pwl.stop();
        pwl = null;
        vc.setVisible( true );
        dispose();
    }//GEN-LAST:event_btn_cargar_todoActionPerformed
    
    private void jLabel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseExited
        jLabel5.setForeground( Color.white );
    }//GEN-LAST:event_jLabel5MouseExited
    
    private void jLabel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseEntered
        jLabel5.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_jLabel5MouseEntered
    
    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        System.exit( 0 );
    }//GEN-LAST:event_jLabel5MouseClicked
    
    private void label_listosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_listosMouseClicked
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        panel_seleccion.setVisible( false );
        panel_cocos.setVisible( true );
        label_jugador_actual.setText( GestorJugadores.getInstancia().jugadorActual().getNombre() );
        pwl.suspend();
        cocos = MainCoco.cargar();
        txt_nombre_coco.setText( buscarCoco().getNombre() );
        txt_atributo_coco.setText(buscarCoco().getEspecialidad().getDescripcion() );
        txt_bio_coco.setText( buscarCoco().getDescripcion() );
        musica_coco = new PlayWaveLoop( buscarCoco().getMusica() );
        musica_coco.start();
    }//GEN-LAST:event_label_listosMouseClicked
    
    private void label_listosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_listosMouseEntered
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_listosMouseEntered
    
    private void label_aceptar1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_aceptar1MouseExited
        label_aceptar1.setForeground( Color.white );
    }//GEN-LAST:event_label_aceptar1MouseExited
    
    private void label_aceptar1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_aceptar1MouseEntered
        label_aceptar1.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_aceptar1MouseEntered
    
    private void label_aceptar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_aceptar1MouseClicked
        label_aceptar1.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        txt_jugador2.setEditable( false );
        continuar();
        if( txt_jugador2.getText().length() != 0 ){
            jugador2 = new Jugador( txt_jugador2.getText() );
            GestorJugadores.getInstancia().agregarJugador( jugador2 );
        }else{
            jugador1 = new Jugador( "Coquisimo" );
            GestorJugadores.getInstancia().agregarJugador( jugador1 );
        }
    }//GEN-LAST:event_label_aceptar1MouseClicked
    
    private void label_aceptarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_aceptarMouseExited
        label_aceptar.setForeground( Color.white );
    }//GEN-LAST:event_label_aceptarMouseExited
    
    private void label_aceptarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_aceptarMouseEntered
        label_aceptar.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_aceptarMouseEntered
    
    private void label_aceptarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_aceptarMouseClicked
        label_aceptar.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        txt_jugador1.setEditable( false );
        continuar();
        if( txt_jugador1.getText().length() != 0 ){
            jugador1 = new Jugador( txt_jugador1.getText() );
            GestorJugadores.getInstancia().agregarJugador( jugador1 );
        }else{
            jugador1 = new Jugador( "Coquito" );
            GestorJugadores.getInstancia().agregarJugador( jugador1 );
        }
    }//GEN-LAST:event_label_aceptarMouseClicked
    
    private void label_volverMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_volverMouseExited
        label_volver.setForeground( Color.white );
    }//GEN-LAST:event_label_volverMouseExited
    
    private void label_volverMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_volverMouseEntered
        label_volver.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_volverMouseEntered
    
    private void label_volverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_volverMouseClicked
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        panel_seleccion.setVisible( false );
        label_listos.setVisible( false );
        txt_jugador1.setEditable( true );
        txt_jugador1.setText( "" );
        txt_jugador2.setEditable( true );
        txt_jugador2.setText( "" );
        panel_menu.setVisible( true );
    }//GEN-LAST:event_label_volverMouseClicked
    
    private void label_spqrMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_spqrMouseExited
        label_spqr.setForeground( Color.white );
    }//GEN-LAST:event_label_spqrMouseExited
    
    private void label_jugarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_jugarMouseExited
        label_jugar.setForeground( Color.white );
    }//GEN-LAST:event_label_jugarMouseExited
    
    private void label_salirMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_salirMouseExited
        label_salir.setForeground( Color.white );
    }//GEN-LAST:event_label_salirMouseExited
    
    private void label_jugarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_jugarMouseEntered
        label_jugar.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_jugarMouseEntered
    
    private void label_jugarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_jugarMouseClicked
        panel_menu.setVisible( false );
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        panel_seleccion.setVisible( true );
    }//GEN-LAST:event_label_jugarMouseClicked
    
    private void label_spqrMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_spqrMouseEntered
        label_spqr.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_spqrMouseEntered
    
    private void label_spqrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_spqrMouseClicked
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        //TODO
        JInternalFrame jInternalFrame1 = new VIAutores();
        Point p = new Point( pantalla.width/2 - jInternalFrame1.getSize().width/2,
                pantalla.height/2 - jInternalFrame1.getSize().height/2 );
        capa_fondo.add(jInternalFrame1, javax.swing.JLayeredPane.DRAG_LAYER);
        jInternalFrame1.setCursor( GestorGrafico.getInstancia().coquismo() );
        jInternalFrame1.setLocation(p);
        jInternalFrame1.setClosable(true);
        jInternalFrame1.validate();
        jInternalFrame1.setVisible( true );
    }//GEN-LAST:event_label_spqrMouseClicked
    
    private void label_salirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_salirMouseClicked
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        System.exit( 0 );
    }//GEN-LAST:event_label_salirMouseClicked
    
    private void label_salirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_salirMouseEntered
        label_salir.setForeground( COLOR );
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_label_salirMouseEntered
    
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        initFullScreen();
        pwl = new PlayWaveLoop( "src/musica/Menu.wav");
        pwl.start();
        Toolkit tk = Toolkit.getDefaultToolkit();
        this.setCursor( GestorGrafico.getInstancia().coquismo() );
        capa_fondo.setCursor( GestorGrafico.getInstancia().coquismo() );
        panel_cartas.setCursor( GestorGrafico.getInstancia().coquismo() );
        panel_cocos.setCursor( GestorGrafico.getInstancia().coquismo() );
        panel_menu.setCursor( GestorGrafico.getInstancia().coquismo() );
        panel_seleccion.setCursor( GestorGrafico.getInstancia().coquismo() );
        initFullScreen();
    }//GEN-LAST:event_formWindowOpened
    
    private void aumentarContadorCoco(){
        if( contador_cocos == 5 ){
            contador_cocos = 1;
        }else{
            contador_cocos++;
        }   //fin del if...else
    }   //fin del metodo aumentarContadorCoco
    
    private void disminuirContadorCoco(){
        if( contador_cocos == 1 ){
            contador_cocos = 5;
        }else{
            contador_cocos--;
        }   //fin del if...else
    }   //fin del metodo disminuirContadorCoco
    
    private Coco buscarCoco(){
        switch( contador_cocos ){
            case 1: return (Coco) cocos.get( "Coco Freeman" );
            case 2: return (Coco) cocos.get( "Coco 70's" );
            case 3: return (Coco) cocos.get( "Coco Bombeable" );
            case 4: return (Coco) cocos.get( "Coco" );
            case 5: return (Coco) cocos.get( "WarCoco" );
            default: return null;
        }   //fin del switch
    }   //fin del metodo buscarCocos
    
    private void continuar(){
        if( !txt_jugador1.isEditable() && !txt_jugador2.isEditable() ){
            label_listos.setVisible( true );
        }   //fin del if
    }   //fin del metodo continuar
    
    private int analizarCartas(){
        switch( box_cantidad_cartas.getSelectedIndex() ){
            case 0: return 12;
            case 1: return 24;
            case 2: return 36;
            case 3: return 48;
            case 4: return 60;
            default: return 0;
        }   //fin del switch
    }   //fin del metodo analizarCartas
    
    private class MueveFondo extends Thread{
        private boolean en_menu;
        private int dir = -1;
        public void run(){
            en_menu = true;
            while( en_menu ){
                label_fondo.setLocation( label_fondo.getLocation().x + dir,
                        label_fondo.getLocation().y );
                try {
                    sleep( 250 );
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                yield();
                if( label_fondo.getSize().width + label_fondo.getLocation().x + dir <
                        pantalla.width ){
                    dir = 1;
                }   //fin del if
                if( label_fondo.getLocation().x + dir > 0 ){
                    dir = -1;
                }   //fin del if
            }   //fin del while
        }   //fin del metodo run
        
        public void parar(){
            en_menu = false;
        }   //fin del metodo parar
    }   //fin de la clase MueveFondo
    
    /**Metodo para hacer el programa en FullScreen. Debe de copiarse en todas
     * las ventanas que se vayan a usar. Debe llamarse el metodo en el constructor
     * del form. @returns: void*/
    private void initFullScreen(){
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        gd = ge.getDefaultScreenDevice();
        
        gd.setFullScreenWindow(this);
        pWidth = getBounds().width;
        pHeight = getBounds().height;
        
    }  // end of initFullScreen()
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox box_cantidad_cartas;
    private javax.swing.JButton btn_aceptar;
    private javax.swing.JButton btn_atras;
    private javax.swing.JButton btn_cargar_todo;
    private javax.swing.JButton btn_sig;
    private javax.swing.JLayeredPane capa_fondo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel label_aceptar;
    private javax.swing.JLabel label_aceptar1;
    private javax.swing.JLabel label_coco;
    private javax.swing.JLabel label_fondo;
    private javax.swing.JLabel label_jugador;
    private javax.swing.JLabel label_jugador1;
    private javax.swing.JLabel label_jugador_actual;
    private javax.swing.JLabel label_jugar;
    private javax.swing.JLabel label_listos;
    private javax.swing.JLabel label_salir;
    private javax.swing.JLabel label_spqr;
    private javax.swing.JLabel label_volver;
    private javax.swing.JPanel panel_cartas;
    private javax.swing.JPanel panel_cocos;
    private javax.swing.JPanel panel_menu;
    private javax.swing.JPanel panel_seleccion;
    private javax.swing.JLabel txt;
    private javax.swing.JTextField txt_atributo_coco;
    private javax.swing.JScrollPane txt_bio;
    private javax.swing.JTextPane txt_bio_coco;
    private javax.swing.JTextField txt_jugador1;
    private javax.swing.JTextField txt_jugador2;
    private javax.swing.JTextField txt_nombre_coco;
    // End of variables declaration//GEN-END:variables
    private GraphicsDevice gd;
    private BufferStrategy bufferStrategy;
    private int pWidth, pHeight;
    private Dimension pantalla;
    private static Color COLOR = new Color(206, 206, 255 );
    private PlayWaveLoop pwl;
    private Jugador jugador1, jugador2;
    private PlayWaveLoop musica_coco;
    private int centinela, contador_cocos;
    private TreeMap cocos;
    private ImageIcon imagen;
}   //fin de la clase VentanaMenu