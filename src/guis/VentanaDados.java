package guis;

import clases.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import javax.swing.ImageIcon;
import utilerias.sonido.AePlayWave;
import utilerias.sonido.PlayWaveLoop;

public class VentanaDados extends javax.swing.JFrame {
    
    /** Creates new form VentanaDados */
    public VentanaDados() {
        initComponents();
        pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        label_fondo.setLocation( (pantalla.width/2 - label_fondo.getSize().width/2),
                (pantalla.height/2 - label_fondo.getSize().height/2) );
        grafico_coco1.setVisible( false );
        grafico_coco2.setVisible( false );
        label_mensaje.setVisible(false);
        jPanel1.setSize(pantalla.width - 10, jPanel1.getSize().height );
        jPanel1.setLocation(10,10);
        
        grafico_coco1.setLocation(10, 250 );
        grafico_coco2.setLocation((pantalla.width - (grafico_coco2.getSize().width + 10)), 250 );
        dado2.setLocation( (pantalla.width - (dado2.getSize().width + 10)),
                pantalla.height - (dado2.getSize().height + 10) );
        
        label_mensaje.setLocation( (pantalla.width/2 - label_mensaje.getSize().width/2),
                (pantalla.height/2 - label_mensaje.getSize().height/2) );
        label_mensaje.setVisible(true);
        dado1.setLocation( 10, pantalla.height - (dado1.getSize().height + 10) );
        btn_continuar.setLocation( (pantalla.width/2 - btn_continuar.getSize().width/2),
                btn_continuar.getLocation().x );
        initTextos();
        fin1 = false;
        fin2 = false;
        btn_continuar.setVisible( false );
        d1 = new NumeroAleatorio1();
        d2 = new NumeroAleatorio2();
        d1.setPriority(Thread.MAX_PRIORITY);
        d2.setPriority(Thread.MAX_PRIORITY);
        d1.start();
        d2.start();
        this.setCursor( GestorGrafico.getInstancia().coquismo() );
        capa_fondo.setCursor( GestorGrafico.getInstancia().coquismo() );
        jPanel1.setCursor( GestorGrafico.getInstancia().coquismo() );
    }   //fin del constructor
    
    /**Metodo para hacer el programa en FullScreen. Debe de copiarse en todas
     * las ventanas que se vayan a usar. Debe llamarse el metodo en el constructor
     * del form. @returns: void*/
    private void initFullScreen(){
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        gd = ge.getDefaultScreenDevice();
        gd.setFullScreenWindow(this);
        pWidth = getBounds().width;
        pHeight = getBounds().height;
    }  // end of initFullScreen()
    
    /**Inicializa todos los textos en pantalla. Para hacer esto, cambia el
     * turno para obtener al jugador 2*/
    private void initTextos(){
        label_coco1.setText( GestorJugadores.getInstancia().jugadorActual().getCoco().getNombre() );
        label_jugador1.setText( GestorJugadores.getInstancia().jugadorActual().getNombre() );
        grafico_coco1.setIcon( new ImageIcon(GestorGrafico.getInstancia().imagenesCocosDerechoGrandes(
                GestorJugadores.getInstancia().jugadorActual().getCoco()) ) );
        grafico_coco1.setVisible( true );
        
        label_coco2.setText( GestorJugadores.getInstancia().jugadorDos().getCoco().getNombre() );
        label_jugador2.setText( GestorJugadores.getInstancia().jugadorDos().getNombre() );
        grafico_coco2.setIcon( new ImageIcon(GestorGrafico.getInstancia().imagenesCocosIzquierdaGrandes(
                GestorJugadores.getInstancia().jugadorDos().getCoco()) ) );
        grafico_coco2.setVisible( true );
    }   //fin del metodo initTextos
    
    /**Analiza si ya se detuvieron los dos dados*/
    private void analizarContinuacion(){
        if( fin1 && fin2 ){
            if (d1.getNumero() == d2.getNumero()){
                new MuestraMensaje().run();
            }else{
                btn_continuar.setVisible( true );
            }
        }   //fin del if
    }   //fin del metood analizarContinuacion
    
    /**Analiza cual de los dos jugadores va primero*/
    private void analizarTurno(){
        if( d1.getNumero() < d2.getNumero() ){
            GestorJugadores.getInstancia().cambiarTurno();
        }
        GestorJugadores.getInstancia().jugadorActual().setNumero_jugador(1);
        GestorJugadores.getInstancia().jugadorDos().setNumero_jugador(2);
    }   //fin del metodo analizarTurno
    
    /**Genera un numero aleatorio para el dado2*/
    private class NumeroAleatorio2 extends Thread{
        private int numero;
        public void run(){
            while( !fin2 ){
                numero = (int) ((Math.random() * 6) + 1);
                dado2.setIcon( new ImageIcon(GestorGrafico.getInstancia().analizarDado(getNumero())) );
                try {
                    sleep(15);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }   //fin del while
        }   //fin del metodo run
        
        public int getNumero() {return numero;}
    }   //fin de la clase
    
    /**Genera un numero aleatorio para el dado1*/
    private class NumeroAleatorio1 extends Thread{
        private int numero;
        public void run(){
            while( !fin1 ){
                numero = (int) ((Math.random() * 6) + 1);
                dado1.setIcon( new ImageIcon(GestorGrafico.getInstancia().analizarDado(numero)) );
                try {
                    sleep(15);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }   //fin del while
        }   //fin del metodo run
        public int getNumero() {return numero;}
    }   //fin de la clase
    
    private class MuestraMensaje extends Thread{
        public void run(){
            label_mensaje.setText( "Numeros iguales. Tirando los dados otra vez.");
            label_mensaje.setLocation( (pantalla.width/2 - label_mensaje.getSize().width/2),
                    (pantalla.height/2 - label_mensaje.getSize().height/2) );
            label_mensaje.setVisible( true );
            try {
                sleep( 3000 );
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }   //fin del try & catch
            label_mensaje.setVisible( false );
            d1 = new NumeroAleatorio1();
            d2 = new NumeroAleatorio2();
            d1.start();
            d2.start();
            fin1 = false;
            fin2 = false;
        }   //fin del run
    }   //fin de la clase MuestraTurno
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        capa_fondo = new javax.swing.JLayeredPane();
        label_mensaje = new javax.swing.JLabel();
        label_fondo = new javax.swing.JLabel();
        grafico_coco1 = new javax.swing.JLabel();
        grafico_coco2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        label_jugador2 = new javax.swing.JLabel();
        label_coco2 = new javax.swing.JLabel();
        label_jugador1 = new javax.swing.JLabel();
        label_coco1 = new javax.swing.JLabel();
        dado1 = new javax.swing.JLabel();
        dado2 = new javax.swing.JLabel();
        btn_continuar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Coco Void");
        setResizable(false);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        label_mensaje.setFont(new java.awt.Font("BaileysCar", 1, 170));
        label_mensaje.setForeground(new java.awt.Color(255, 0, 0));
        label_mensaje.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_mensaje.setText("VS");
        label_mensaje.setPreferredSize(new java.awt.Dimension(300, 300));
        label_mensaje.setBounds(530, 310, 220, 300);
        capa_fondo.add(label_mensaje, javax.swing.JLayeredPane.DRAG_LAYER);

        label_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Intro.jpg")));
        label_fondo.setBounds(0, 0, 1280, 1024);
        capa_fondo.add(label_fondo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        grafico_coco1.setFocusable(false);
        grafico_coco1.setInheritsPopupMenu(false);
        grafico_coco1.setRequestFocusEnabled(false);
        grafico_coco1.setVerifyInputWhenFocusTarget(false);
        grafico_coco1.setBounds(10, 300, 400, 300);
        capa_fondo.add(grafico_coco1, javax.swing.JLayeredPane.POPUP_LAYER);

        grafico_coco2.setFocusable(false);
        grafico_coco2.setInheritsPopupMenu(false);
        grafico_coco2.setRequestFocusEnabled(false);
        grafico_coco2.setVerifyInputWhenFocusTarget(false);
        grafico_coco2.setBounds(850, 300, 400, 300);
        capa_fondo.add(grafico_coco2, javax.swing.JLayeredPane.POPUP_LAYER);

        jPanel1.setFocusable(false);
        jPanel1.setOpaque(false);
        jPanel1.setRequestFocusEnabled(false);
        jPanel1.setVerifyInputWhenFocusTarget(false);
        label_jugador2.setForeground(new java.awt.Color(0, 102, 255));
        label_jugador2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_jugador2.setText("Jugador");

        label_coco2.setFont(new java.awt.Font("Dialog", 1, 48));
        label_coco2.setForeground(new java.awt.Color(51, 102, 255));
        label_coco2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_coco2.setText("Coco");

        label_jugador1.setForeground(new java.awt.Color(204, 204, 204));
        label_jugador1.setText("Jugador");

        label_coco1.setFont(new java.awt.Font("Dialog", 1, 48));
        label_coco1.setForeground(new java.awt.Color(204, 204, 204));
        label_coco1.setText("Coco");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(label_coco1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 552, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 188, Short.MAX_VALUE)
                        .add(label_coco2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 496, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .add(label_jugador1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 286, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 619, Short.MAX_VALUE)
                        .add(label_jugador2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 331, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(label_jugador2)
                    .add(label_jugador1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(label_coco2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(label_coco1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(36, 36, 36))
        );
        jPanel1.setBounds(10, 100, 1260, 110);
        capa_fondo.add(jPanel1, javax.swing.JLayeredPane.PALETTE_LAYER);

        dado1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/dados/uno.gif")));
        dado1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dado1MouseClicked(evt);
            }
        });

        dado1.setBounds(10, 750, 163, 173);
        capa_fondo.add(dado1, javax.swing.JLayeredPane.MODAL_LAYER);

        dado2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/dados/uno.gif")));
        dado2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dado2MouseClicked(evt);
            }
        });

        dado2.setBounds(10, 750, 163, 173);
        capa_fondo.add(dado2, javax.swing.JLayeredPane.MODAL_LAYER);

        btn_continuar.setBackground(new java.awt.Color(102, 102, 102));
        btn_continuar.setForeground(new java.awt.Color(255, 255, 255));
        btn_continuar.setText("Continuar");
        btn_continuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_continuarActionPerformed(evt);
            }
        });

        btn_continuar.setBounds(570, 210, 96, 25);
        capa_fondo.add(btn_continuar, javax.swing.JLayeredPane.PALETTE_LAYER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(capa_fondo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1280, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(capa_fondo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 809, Short.MAX_VALUE)
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void dado2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dado2MouseClicked
        fin2 = true;
        analizarContinuacion();
    }//GEN-LAST:event_dado2MouseClicked
    
    private void dado1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dado1MouseClicked
        fin1 = true;
        analizarContinuacion();
    }//GEN-LAST:event_dado1MouseClicked
    
    private void btn_continuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_continuarActionPerformed
        VentanaTablero vt = new VentanaTablero();
        analizarTurno();
        pwl.stop();
        vt.setVisible( true );
        dispose();
    }//GEN-LAST:event_btn_continuarActionPerformed
    
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        initFullScreen();
        pwl = new PlayWaveLoop( "src/musica/efectos/intro.wav");
        pwl.start();
    }//GEN-LAST:event_formWindowOpened
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_continuar;
    private javax.swing.JLayeredPane capa_fondo;
    private javax.swing.JLabel dado1;
    private javax.swing.JLabel dado2;
    private javax.swing.JLabel grafico_coco1;
    private javax.swing.JLabel grafico_coco2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel label_coco1;
    private javax.swing.JLabel label_coco2;
    private javax.swing.JLabel label_fondo;
    private javax.swing.JLabel label_jugador1;
    private javax.swing.JLabel label_jugador2;
    private javax.swing.JLabel label_mensaje;
    // End of variables declaration//GEN-END:variables
    private GraphicsDevice gd;
    private BufferStrategy bufferStrategy;
    private int pWidth, pHeight;
    private Dimension pantalla;
    private boolean fin1, fin2;
    private NumeroAleatorio1 d1;
    private NumeroAleatorio2 d2;
    private PlayWaveLoop pwl;
}   //fin de la clase VentanaDados