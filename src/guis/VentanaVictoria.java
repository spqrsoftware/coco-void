package guis;

import clases.GestorCartas;
import clases.GestorGrafico;
import clases.GestorJugadores;
import clases.otros.Jugador;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import javax.swing.ImageIcon;
import utilerias.sonido.AePlayWave;

public class VentanaVictoria extends javax.swing.JFrame {
    
    /** Creates new form VentanaVictoria */
    public VentanaVictoria() {
        initComponents();
        pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        capa_fondo.setSize( pantalla.width, pantalla.height );
        label_victoria.setVisible( false );
        label_victoria.setLocation( 1, pantalla.height + 1 );
        panel_info.setLocation( 10, 10 );
    }   //fin del contructor
    
    /**Metodo para hacer el programa en FullScreen. Debe de copiarse en todas
     * las ventanas que se vayan a usar. Debe llamarse el metodo en el constructor
     * del form. @returns: void*/
    private void initFullScreen(){
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        gd = ge.getDefaultScreenDevice();
        gd.setFullScreenWindow(this);
        pWidth = getBounds().width;
        pHeight = getBounds().height;
        
    }  // end of initFullScreen()
    
    private void initTextos(){
        Jugador j = GestorJugadores.getInstancia().getGanador();
        label_nombre.setText( j.getNombre() );
        label_nombre_coco.setText( j.getCoco().getNombre() );
        label_puntaje.setText( String.valueOf(j.getCoco().getPuntaje()) );
        label_posicion.setText( String.valueOf(j.getCoco().getPosicion()) );
        grafico_coco.setIcon( new ImageIcon( GestorGrafico.getInstancia().
                imagenesCocosFrente(j.getCoco())) );
        grafico_salida_coco.setIcon( new ImageIcon( GestorGrafico.getInstancia().
                imagenesCocosIzquierdaPeque(j.getCoco())) );
    }   //fin del metodo initTextos
    
    private class Creditos extends Thread{
        public void run(){
            label_victoria.setVisible(true);
            new AePlayWave("src/musica/Menu.wav").start();
            for( int i = 0; i < 9; i++ ){
                asignarTexto( i );
                yield();
                for( int j = 0; j <= pantalla.height + label_victoria.getSize().height; j++ ){
                    label_victoria.setLocation( label_victoria.getLocation().x,
                            label_victoria.getLocation().y - 1 );
                    try {
                        sleep(2);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }   //fin del for j
            }   //fin del for i
            System.exit(0);
        }   //fin del metodo run
        
        private void asignarTexto(int i){
            label_victoria.setLocation( pantalla.width/2 - label_victoria.getSize().width/2,
                pantalla.height + 1 );
            switch( i ){
                case 1: label_victoria.setText( "...:::  SPQR SOFTWARE  :::...");
                        label_victoria.setIcon( null );
                break;
                case 2: label_victoria.setText( "Pious (Pablo Peraza)");
                break;
                case 3: label_victoria.setText( "Londo (Eduardo Obando)");
                break;
                case 4: label_victoria.setText( "Arkhon (Alberto Campos)");
                break;
                case 0: label_victoria.setText( "" );
                        label_victoria.setIcon( new ImageIcon( "src/imagenes/logo.jpg"));
                break;
                case 5: label_victoria.setText( "Agradecemos a Jonathan Orozco por encocarse");                        
                break;
                case 6: label_victoria.setText( "Ningun coco fue herido durante la realizacion de este Juego");
                break;
                case 7: label_victoria.setText( "SPQR Software 2006");
                        label_victoria.setIcon( new ImageIcon( "src/imagenes/SPQR logo.jpg"));
                break;
                case 8: label_victoria.setText( "Universidad Latina de Costa Rica");     
                        label_victoria.setIcon( null );
            }   //fin del switch
        }   //fin del metodo asignarTexto
    }   //fin de la clase Creditos
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        capa_fondo = new javax.swing.JLayeredPane();
        grafico_fondo = new javax.swing.JLabel();
        label_victoria = new javax.swing.JLabel();
        panel_info = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        label_nombre = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        label_nombre_coco = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        label_puntaje = new javax.swing.JLabel();
        btn_continuar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        label_posicion = new javax.swing.JLabel();
        grafico_coco = new javax.swing.JLabel();
        btn_reiniciar = new javax.swing.JButton();
        grafico_salida_coco = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Coco Void");
        setResizable(false);
        setUndecorated(true);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                formKeyTyped(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        capa_fondo.setBackground(new java.awt.Color(255, 255, 255));
        capa_fondo.setFocusable(false);
        capa_fondo.setRequestFocusEnabled(false);
        capa_fondo.setVerifyInputWhenFocusTarget(false);
        grafico_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Victoria copy.jpg")));
        grafico_fondo.setFocusable(false);
        grafico_fondo.setRequestFocusEnabled(false);
        grafico_fondo.setVerifyInputWhenFocusTarget(false);
        grafico_fondo.setBounds(0, 0, 1280, 1024);
        capa_fondo.add(grafico_fondo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        label_victoria.setFont(new java.awt.Font("Credit River", 1, 48));
        label_victoria.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_victoria.setText("Creditos");
        label_victoria.setFocusable(false);
        label_victoria.setRequestFocusEnabled(false);
        label_victoria.setVerifyInputWhenFocusTarget(false);
        label_victoria.setBounds(50, 60, 1180, 210);
        capa_fondo.add(label_victoria, javax.swing.JLayeredPane.MODAL_LAYER);

        panel_info.setBackground(new java.awt.Color(102, 102, 102));
        panel_info.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Jugador:");

        label_nombre.setFont(new java.awt.Font("Tahoma", 1, 36));
        label_nombre.setForeground(new java.awt.Color(153, 255, 0));
        label_nombre.setText("Nombre");

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Coco:");

        label_nombre_coco.setFont(new java.awt.Font("Tahoma", 1, 36));
        label_nombre_coco.setForeground(new java.awt.Color(153, 255, 0));
        label_nombre_coco.setText("Coco");

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Puntaje:");

        label_puntaje.setFont(new java.awt.Font("Tahoma", 1, 36));
        label_puntaje.setForeground(new java.awt.Color(153, 255, 0));
        label_puntaje.setText("0");

        btn_continuar.setBackground(new java.awt.Color(0, 0, 0));
        btn_continuar.setForeground(new java.awt.Color(255, 255, 255));
        btn_continuar.setText("Continuar");
        btn_continuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_continuarActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Posici\u00f3n:");

        label_posicion.setFont(new java.awt.Font("Tahoma", 1, 36));
        label_posicion.setForeground(new java.awt.Color(153, 255, 0));
        label_posicion.setText("0");

        grafico_coco.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btn_reiniciar.setBackground(new java.awt.Color(0, 0, 0));
        btn_reiniciar.setForeground(new java.awt.Color(255, 255, 255));
        btn_reiniciar.setText("Reiniciar");
        btn_reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reiniciarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panel_infoLayout = new org.jdesktop.layout.GroupLayout(panel_info);
        panel_info.setLayout(panel_infoLayout);
        panel_infoLayout.setHorizontalGroup(
            panel_infoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_infoLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_infoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panel_infoLayout.createSequentialGroup()
                        .add(10, 10, 10)
                        .add(label_nombre_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 294, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(panel_infoLayout.createSequentialGroup()
                        .add(10, 10, 10)
                        .add(label_puntaje, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 294, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(panel_infoLayout.createSequentialGroup()
                        .add(10, 10, 10)
                        .add(label_nombre, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
                    .add(jLabel1)
                    .add(jLabel2)
                    .add(jLabel3)
                    .add(panel_infoLayout.createSequentialGroup()
                        .add(panel_infoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(panel_infoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(label_posicion, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(btn_continuar)
                            .add(btn_reiniciar))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(grafico_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panel_infoLayout.setVerticalGroup(
            panel_infoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_infoLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(label_nombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(39, 39, 39)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(label_nombre_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(jLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(label_puntaje, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_infoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(panel_infoLayout.createSequentialGroup()
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(label_posicion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 77, Short.MAX_VALUE)
                        .add(btn_reiniciar)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btn_continuar))
                    .add(grafico_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 188, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        panel_info.setBounds(10, 10, 330, 500);
        capa_fondo.add(panel_info, javax.swing.JLayeredPane.MODAL_LAYER);

        grafico_salida_coco.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grafico_salida_coco.setBounds(570, 260, 140, 210);
        capa_fondo.add(grafico_salida_coco, javax.swing.JLayeredPane.MODAL_LAYER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(capa_fondo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1283, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(capa_fondo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 1022, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_reiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reiniciarActionPerformed
        GestorJugadores.getInstancia().limpiar();
        GestorCartas.getInstancia().limpiar();
        VentanaMenu vm = new VentanaMenu();
        vm.setVisible( true );
        dispose();
    }//GEN-LAST:event_btn_reiniciarActionPerformed

    private void btn_continuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_continuarActionPerformed
        new Creditos().start();
        panel_info.setVisible( false );
    }//GEN-LAST:event_btn_continuarActionPerformed

    private void formKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyTyped
        if( evt.getKeyCode() == KeyEvent.VK_ESCAPE ){
            System.exit(0);
        } 
    }//GEN-LAST:event_formKeyTyped

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        initFullScreen();
        initTextos();
    }//GEN-LAST:event_formWindowOpened
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_continuar;
    private javax.swing.JButton btn_reiniciar;
    private javax.swing.JLayeredPane capa_fondo;
    private javax.swing.JLabel grafico_coco;
    private javax.swing.JLabel grafico_fondo;
    private javax.swing.JLabel grafico_salida_coco;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel label_nombre;
    private javax.swing.JLabel label_nombre_coco;
    private javax.swing.JLabel label_posicion;
    private javax.swing.JLabel label_puntaje;
    private javax.swing.JLabel label_victoria;
    private javax.swing.JPanel panel_info;
    // End of variables declaration//GEN-END:variables
    private GraphicsDevice gd;
    private BufferStrategy bufferStrategy;
    private int pWidth, pHeight;
    private Dimension pantalla;
}   //fin de la clase VentanaVictoria