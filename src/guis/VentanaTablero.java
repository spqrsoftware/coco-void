package guis;

import clases.Carta.*;
import clases.*;
import clases.otros.Coco;
import clases.otros.Jugador;
import java.awt.*;
import java.awt.image.BufferStrategy;
import javax.swing.ImageIcon;
import utilerias.sonido.*;

public class VentanaTablero extends javax.swing.JFrame {
    
    /** Creates new form VentanaTablero */
    public VentanaTablero() {
        initComponents();
        label_fondo.setIcon( new ImageIcon( "src/imagenes/Bulto.jpg") );
        centinela = false;
        tomo_carta = false;
        gano = false;
        pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        mapa = capa_tablero.getSize();
        mp = new MuevePantalla();
        mp.setDaemon( true );
        mp.setPriority( Thread.MIN_PRIORITY + 1);
        mp.start();
        panel_carta_defensa.setVisible( false );
        panel_guerra.setVisible( false );
        label_poder.setVisible( false );
        label_puntaje.setVisible( false );
        label_movimiento.setVisible( false );
        label_poder_carta_defensa.setVisible( false );
        label_puntaje_carta_defensa.setVisible( false );
        btn_salir.setLocation( pantalla.width - (btn_salir.getSize().width + 10),
                btn_salir.getSize().height + 10 );
        label_mostrar_turno.setLocation( (pantalla.width/2 - label_mostrar_turno.getSize().width/2),
                (pantalla.height/2 - label_mostrar_turno.getSize().height/2) );
        panel_carta_defensa.setLocation( (pantalla.width/2 - panel_carta_defensa.getSize().width/2),
                10 );
        panel_guerra.setLocation( (pantalla.width/2 - panel_guerra.getSize().width/2),
                10 );
        label_mostrar_turno.setVisible( false );
        grafico_carta_tomada.setVisible( false );
        panel_menu.setSize( pantalla.width, (int)(pantalla.height * 0.30) );
        panel_menu.setLocation( 0, pantalla.height - panel_menu.getSize().height );
        capa_fondo.setCursor( GestorGrafico.getInstancia().coquismo() );
        capa_tablero.setCursor( GestorGrafico.getInstancia().coquismo() );
        initMusica();
    }   //fin del constructor
    
    // <editor-fold defaultstate="collapsed" desc=" metodo initFullScreen ">
    /**Metodo para hacer el programa en FullScreen. Debe de copiarse en todas
     * las ventanas que se vayan a usar. Debe llamarse el metodo en el constructor
     * del form. @returns: void*/
    private void initFullScreen(){
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        gd = ge.getDefaultScreenDevice();
        
        gd.setFullScreenWindow(this);
        pWidth = getBounds().width;
        pHeight = getBounds().height;
        
    }  // end of initFullScreen()
    //</editor-fold>
    
    private void initMusica(){
        if( GestorJugadores.getInstancia().jugadorActual().getNumero_jugador() == 1 ){
            musica = new PlayWaveLoop( GestorJugadores.getInstancia().jugadorActual().getCoco().getMusica() );
            musica.start();
            musica.suspend();
            musica2 = new PlayWaveLoop( GestorJugadores.getInstancia().jugadorDos().getCoco().getMusica());
            musica2.start();
            musica2.suspend();
        }else{
            musica2 = new PlayWaveLoop( GestorJugadores.getInstancia().jugadorActual().getCoco().getMusica() );
            musica2.start();
            musica2.suspend();
            musica = new PlayWaveLoop( GestorJugadores.getInstancia().jugadorDos().getCoco().getMusica());
            musica.start();
            musica.suspend();
        }
    }   //fin del metodo initMuscia
    
    /**
     *  Asigna el grafico adecuado para cada jugador
     */
    private void initGraficosCocos(){
        grafico_coco1.setIcon( new ImageIcon(
                GestorGrafico.getInstancia().imagenesCocosDerechoPeque(
                GestorJugadores.getInstancia().jugadorActual().getCoco())) );
        grafico_coco2.setIcon( new ImageIcon(
                GestorGrafico.getInstancia().imagenesCocosDerechoPeque(
                GestorJugadores.getInstancia().jugadorDos().getCoco())) );
    }   //fin del metodo initGraficosCocos
    
    
    /**
     *  Se encarga de todas las acciones a tomar cuando se inicia el turno de un
     *  jugador. Primero, despleiga un mensaje que muestra de quien es el turno.
     *  Luego, actualiza todos los labels en el menu, para que correspondan al
     *  jugador actual. Por ultimo, inicia la musica del coco del jugador
     */
    private void initTurno(){
        new MuestraTurno().start(); //muestra el mensaje que despliega el turno
        label_jugador.setText( GestorJugadores.getInstancia().jugadorActual().getNombre() );
        label_nombre_coco.setText(
                GestorJugadores.getInstancia().jugadorActual().getCoco().getNombre() );
        label_puntos.setText(
                String.valueOf(GestorJugadores.getInstancia().jugadorActual().getCoco().getPuntaje()) );
        if( GestorJugadores.getInstancia().jugadorActual().getCoco().getPuntaje() < 0 ){
            label_puntos.setForeground( Color.RED );
        }   //fin del if
        if( GestorJugadores.getInstancia().jugadorActual().getCoco().getPuntaje() == 0 ){
            label_puntos.setForeground( Color.YELLOW );
        }   //fin del if
        if( GestorJugadores.getInstancia().jugadorActual().getCoco().getPuntaje() > 0 ){
            label_puntos.setForeground( Color.WHITE );
        }   //fin del if
        label_especialidad.setText(
                GestorJugadores.getInstancia().jugadorActual().
                getCoco().getEspecialidad().getDescripcion() );
        retrato_coco.setIcon( new ImageIcon(GestorGrafico.getInstancia().retratosCocos(
                GestorJugadores.getInstancia().jugadorActual().getCoco())) );
        if( GestorJugadores.getInstancia().jugadorActual().getNumero_jugador() == 1 ){
            musica2.suspend();
            musica.resume();
        }else{
            musica.suspend();
            musica2.resume();
        }
    }   //fin del metodo initTurno
    
    /**
     *  Se encarga de hacer todo lo referente a la terminacion de un turno. Se
     *  puede decir que hace lo contrario al metodo initTurno. Tambien llama al
     *  metodo analizadorDeVictoria para ver si se ha llegado a una
     *  condicion de gane. Devuelve todas las variables cambiadas por el turno,
     *  a su estado original.
     *
     *  @see: analizadorDeVictoria
     */
    private void cambiarTurno(){
        GestorJugadores.getInstancia().cambiarTurno();
        analizadorDeVictoria();
        initTurno();
        tomo_carta = false;
        grafico_carta.setIcon( new ImageIcon("src/imagenes/tarjetas/BackCard.jpg" ));
    }   //fin del metodo cambiarTurno
    
    /**
     *  Se encarga de todas las acciones que hay que realizar, cuando el jugador
     *  toma una carta. Primero, se fija que el jugador no haya tomado una carta
     *  en el mismo turno.
     */
    private void tomarCarta(){
        if( !tomo_carta ){
            analizadorDeVictoria();
            new AePlayWave( "src/musica/efectos/click.wav" ).start();
            carta_actual = GestorCartas.getInstancia().tomarCarta();
            grafico_carta_tomada.setIcon( new ImageIcon(carta_actual.getFrente()) );
            GestorJugadores.getInstancia().jugadorActual().getCoco().setPuntaje( carta_actual.getPuntaje() );
            analizarAccionATomar();
            
        }   //fin del !tomo_carta
    }   //fin del metodo tomarCarta
    
    /**
     *  Analiza el tipo de carta que salio al tomar una carta del maso de cartas,
     *  e indica la accion a tomar para dicho tipo de carta.
     *
     *  @see valoresCartaPanelDefensa
     */
    private void analizarAccionATomar(){
        
        /*Para cartas de defensa*/
        if( carta_actual instanceof CartaDefensa ){
            grafico_carta_defensa.setIcon( grafico_carta_tomada.getIcon() );
            valoresCartaPanelDefensa(); //despleiga los valores de la carta de defensa
            panel_carta_defensa.setVisible(true);
            mostrarValoresCartaActual();
            tomo_carta = true;
            grafico_carta.setIcon( new ImageIcon("src/imagenes/tarjetas/BackCard Gris.jpg" ));
        }   //fin del if
        
        /*Para cartas de avance*/
        if( carta_actual instanceof CartaAvance ){
            new MuestraCarta().start();
            new AePlayWave( "src/musica/efectos/carta_avance.wav").start();
            new MoverCoco2(GestorJugadores.getInstancia().jugadorActual().getNumero_jugador()).start();
        }   //fin del if
        
        /*Para cartas de retroceso*/
        if( carta_actual instanceof CartaRetroceso ){
            if( GestorJugadores.getInstancia().estaVaciaJugadorUno() ){ //si el jugador no tiene cartas de defensa
                new MuestraCarta().start();
                new AePlayWave( "src/musica/efectos/retroceso.wav").start();
                new MoverCoco2(GestorJugadores.getInstancia().jugadorActual().getNumero_jugador()).start();
            }else{  //si el jugador TIENE cartas de defensa
                
                grafico_carta_mala.setIcon( new ImageIcon(carta_actual.getFrente()) );
                temporal = GestorJugadores.getInstancia().jugadorActual().getCoco().verCarta();
                grafico_carta_buena.setIcon( new ImageIcon(temporal.getFrente()) );
                
                valoresPanelGuerra();   //asigna los valores a las cartas
                panel_guerra.setVisible( true );
                mostrarValoresCartaActual();    //despliega los valores
            }   //fin del if...else
        }   //fin del if de cartaretroceso
        
        /*Para cartas de castigo*/
        if( carta_actual instanceof CartaCastigo ){
            new MuestraCarta().start();
            new AePlayWave( "src/musica/efectos/ganar.wav").start();
            new MoverCoco2( GestorJugadores.getInstancia().jugadorDos().getNumero_jugador()).start();
        }   //fin del if
    }   //fin del metodo analizarAccionATomar
    
    /**
     *  Termina el juego.
     */
    private void terminar(){
        gano = true;
        musica.stop();
        musica = null;
        musica2.stop();
        musica2 = null;
        mp.stop();
        mp = null;
        VentanaVictoria vv;
        vv = new VentanaVictoria();
        vv.setVisible(true);
        dispose();
    }   //fin del metodo terminar
    
    // <editor-fold defaultstate="collapsed" desc=" metodos para mostrar cartas ">
    /**
     *  Despliega todos los valores para una carta a mostrar nomalmente
     */
    private void mostrarValoresCartaActual(){
        label_poder.setVisible(true);
        label_puntaje.setVisible(true);
        if( !(carta_actual instanceof CartaDefensa ) )
            label_movimiento.setVisible(true);
    }   //fin del metodo mostrarValoresCarta
    
    /**
     *  Oculta todos los valores para una carta a mostrar nomalmente
     */
    private void ocultarValoresCartaActual(){
        label_poder.setVisible(false);
        label_puntaje.setVisible(false);
        label_movimiento.setVisible(false);
    }   //fin del metodo ocultarValoresCarta
    
    
    /**
     *  Asigno todos los valores para cualquier tipo de carta a mostrar normalmente
     */
    private void valoresCartaActual() {
        int x = grafico_carta_tomada.getLocation().x;
        int y = grafico_carta_tomada.getLocation().y;
        label_poder.setLocation( x + carta_actual.getPosiciones(0).x,
                y + carta_actual.getPosiciones(0).y );
        label_puntaje.setLocation( x + carta_actual.getPosiciones(1).x,
                y + carta_actual.getPosiciones(1).y );
        label_movimiento.setLocation( x + carta_actual.getPosiciones(2).x,
                y + carta_actual.getPosiciones(2).y );
        label_poder.setText( String.valueOf(carta_actual.getPoder() ));
        label_puntaje.setText( String.valueOf(carta_actual.getPuntaje() ));
        label_movimiento.setText( String.valueOf(carta_actual.getMovimiento() ));
    }   //fin del metodo valoresCartas
    
    /**
     *  Asigna todos llos valores para la carta de defensa que se obtuvo del maso.
     *  Debe llamarse antes de mostrar la carta
     */
    private void valoresCartaPanelDefensa(){
        int x = panel_carta_defensa.getLocation().x + grafico_carta_defensa.getLocation().x;
        int y = panel_carta_defensa.getLocation().y + grafico_carta_defensa.getLocation().y;
        label_poder.setLocation( x + carta_actual.getPosiciones(0).x,
                y + carta_actual.getPosiciones(0).y );
        label_puntaje.setLocation( x + carta_actual.getPosiciones(1).x,
                y + carta_actual.getPosiciones(1).y );
        label_poder.setText( String.valueOf(carta_actual.getPoder() ));
        label_puntaje.setText( String.valueOf(carta_actual.getPuntaje() ));
    }   //fin del metodo valoresCartaPanel
    
    /**
     *  Asigna todos los valores para las cartas a mostrar cuando hay un enfrentamiento
     *  entre una carta de retroceso y una carta de defensa
     */
    private void valoresPanelGuerra(){
        int x_malo = panel_guerra.getLocation().x + grafico_carta_mala.getLocation().x;
        int y_malo = panel_guerra.getLocation().y +  grafico_carta_mala.getLocation().y;
        int x_buena = panel_guerra.getLocation().x +  grafico_carta_buena.getLocation().x;
        int y_buena = panel_guerra.getLocation().y +  grafico_carta_buena.getLocation().y;
        
        label_poder.setLocation( x_malo + carta_actual.getPosiciones(0).x,
                y_malo + carta_actual.getPosiciones(0).y );
        label_puntaje.setLocation( x_malo + carta_actual.getPosiciones(1).x,
                y_malo + carta_actual.getPosiciones(1).y );
        label_movimiento.setLocation( x_malo + carta_actual.getPosiciones(2).x,
                y_malo + carta_actual.getPosiciones(2).y );
        label_poder.setText( String.valueOf(carta_actual.getPoder() ));
        label_puntaje.setText( String.valueOf(carta_actual.getPuntaje() ));
        label_movimiento.setText( String.valueOf(carta_actual.getMovimiento() ));
        
        if( temporal != null ){
            label_poder_carta_defensa.setLocation( x_buena + temporal.getPosiciones(0).x,
                    y_buena + temporal.getPosiciones(0).y);
            label_puntaje_carta_defensa.setLocation( x_buena + temporal.getPosiciones(1).x,
                    y_buena + temporal.getPosiciones(1).y);
            label_poder_carta_defensa.setText(String.valueOf(temporal.getPoder() ));
            label_puntaje_carta_defensa.setText(String.valueOf(temporal.getPuntaje() ));
        }
    }   //fin del metodo valoresPanelGuerra
    
    private void mostrarValoresGuerra(){
        label_poder.setVisible( true );
        label_puntaje.setVisible( true );
        label_movimiento.setVisible( true );
        label_poder_carta_defensa.setVisible( true );
        label_puntaje_carta_defensa.setVisible( true );
    }   //fin del metodo mostrarValoresGuerra
    
    private void ocultarValoresGuerra(){
        label_poder.setVisible( false );
        label_puntaje.setVisible( false );
        label_movimiento.setVisible( false );
        label_poder_carta_defensa.setVisible( false );
        label_puntaje_carta_defensa.setVisible( false );
    }   //fin del metodo mostrarValoresGuerra
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" metodo graficoParaMovimiento ">
    private String graficoParaMovimiento(int destino, Coco c){
        int pos_actual = c.getPosicion();
        if( destino <= 0 ){
            return GestorGrafico.getInstancia().imagenesCocosFrente(c);
        }   //fin del if
        if( dePosicionACasilla(pos_actual).x < dePosicionACasilla(destino).x ){
            return GestorGrafico.getInstancia().imagenesCocosDerechoPeque(c);
        }   //fin del if
        
        if( dePosicionACasilla(pos_actual).x > dePosicionACasilla(destino).x ){
            return GestorGrafico.getInstancia().imagenesCocosIzquierdaPeque(c);
        }   //fin del if
        if( dePosicionACasilla(pos_actual).y < dePosicionACasilla(destino).y ){
            return GestorGrafico.getInstancia().imagenesCocosFrente(c);
        }   //fin del if
        if( dePosicionACasilla(pos_actual).y > dePosicionACasilla(destino).y ){
            return GestorGrafico.getInstancia().imagenesCocosEspalda(c);
        }   //fin del if
        return "";
    }   //fin del metodo analizarMovimiento
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" metodo analizadorDeVictoria ">
    /**Analiza si se ha cumplido alguna de las condiciones de victoria. Estas pueden
     *ser: a) Que un jugador llegue a la meta,
     *b) Que se acabe el mazo de cartas.
     *En caso de que se acabe el mazo de cartas, ganara el que esté en una posicion
     *mas adeltanda, y si ambos se encuentran en la misma posicion, ganara el que
     *tenga mayor puntaje. Si aun siguen empatados, ganara el jugador 1*/
    private void analizadorDeVictoria(){
        if(!gano){
            Jugador uno = GestorJugadores.getInstancia().jugadorActual();
            Jugador dos = GestorJugadores.getInstancia().jugadorDos();
            
            if( GestorJugadores.getInstancia().jugadorActual().getCoco().getPosicion()
            >= 27 ){
                GestorJugadores.getInstancia().setGanador( uno );
                terminar();
            }   //fin del if
            
            if( GestorCartas.getInstancia().isVacio() ){
                if( uno.getCoco().getPosicion() > dos.getCoco().getPosicion() ){
                    GestorJugadores.getInstancia().setGanador( uno );
                    terminar();
                }   //fin del if
                
                if( uno.getCoco().getPosicion() < dos.getCoco().getPosicion() ){
                    GestorJugadores.getInstancia().setGanador( dos );
                    terminar();
                }   //fin del if
                
                if( uno.getCoco().getPosicion() == dos.getCoco().getPosicion() ){
                    if( uno.getCoco().getPuntaje() < dos.getCoco().getPuntaje() ){
                        GestorJugadores.getInstancia().setGanador( dos );
                        terminar();
                    }else{
                        GestorJugadores.getInstancia().setGanador( uno );
                        terminar();
                        
                    }   //fin del if...else
                }   //fin del if
            }   //fin del if
            uno = null;
            dos = null;
        }   //fin del if gano
    }   //fin del metodo analizadorDeVictoria
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" metodo dePosicionACasilla">
    /**Convierte la posicion en la que se encuentra el jugador, que es un int,
     *a la posicion relativa de las casillas en el tablero.
     *@parameter: int posicion
     *@return Point posicion*/
    private Point dePosicionACasilla( int p ){
        switch( p ){
            case 1: return casilla1.getLocation();case 2: return casilla2.getLocation();
            case 3: return casilla3.getLocation();case 4: return casilla4.getLocation();
            case 5: return casilla5.getLocation();case 6: return casilla6.getLocation();
            case 7: return casilla7.getLocation();case 8: return casilla8.getLocation();
            case 9: return casilla9.getLocation();case 10: return casilla10.getLocation();
            case 11: return casilla11.getLocation();case 12: return casilla12.getLocation();
            case 13: return casilla13.getLocation();case 14: return casilla14.getLocation();
            case 15: return casilla15.getLocation();case 16: return casilla16.getLocation();
            case 17: return casilla17.getLocation();case 18: return casilla18.getLocation();
            case 19: return casilla19.getLocation();case 20: return casilla20.getLocation();
            case 21: return casilla21.getLocation();case 22: return casilla22.getLocation();
            case 23: return casilla23.getLocation();case 24: return casilla24.getLocation();
            case 25: return casilla25.getLocation();case 26: return casilla26.getLocation();
            case 27: return casilla27.getLocation();default: return panel_salida.getLocation();
        }   //fin del switch
    }   //fin del metodo dePosicionACasilla
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Hilos ">
    // <editor-fold defaultstate="collapsed" desc=" Clase MuevePantalla ">
    /**Clase que sirve para mover el mapa segun sea la posicion del puntero. Extiende
     *de la clase hilo para mover el mapa suavemente.*/
    private class MuevePantalla extends Thread{
        private int dir_horizontal = 0;
        private int dir_vertical = 0;
        
        public void run(){
            while( true ){
                while( centinela ){
                    if( analizar() ){
                        capa_tablero.setLocation( capa_tablero.getLocation().x + dir_horizontal,
                                capa_tablero.getLocation().y + dir_vertical );
                        try {
                            sleep(5);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        yield();
                    }   //fin del if
                }   //fin del while anidado
            }   //fin del while
        }   //fin del metodo run
        
        /**Analiza si la capa del mapa, esta en el borde de la pantalla. De ser asi,
         *el metodo impide que se continue o se realice el movimiento. En caso contrario,
         *permite el movimiento del mapa.*/
        private boolean analizar(){
            if( capa_tablero.getLocation().x + dir_horizontal > 0 ){
                return false;
            }   //analiza el lado izquierdo
            if( (mapa.width + dir_horizontal + capa_tablero.getLocation().x ) < pantalla.width ){
                return false;
            }   //analiza el lado derecho
            if( capa_tablero.getLocation().y + dir_vertical > 0 ){
                return false;
            }   //analiza la parte superior
            if( ( mapa.height + dir_vertical + capa_tablero.getLocation().y ) <
                    (pantalla.height - panel_menu.getSize().height) ){
                return false;
            }   //analiza la parte inferior
            return true;
        }   //fin del metodo analizar
        
        public void setDir_horizontal(String dir) {
            if( "Derecha".equalsIgnoreCase( dir ) ){
                dir_horizontal = -5;
                return;
            }
            if( "Izquierda".equalsIgnoreCase( dir ) ){
                dir_horizontal = 5;
                return;
            }
            dir_horizontal = 0;
        }   //fin del metodo setDir_horizontal
        
        public void setDir_vertical(String dir) {
            if( "Abajo".equalsIgnoreCase( dir ) ){
                dir_vertical = -5;
                return;
            }
            if( "Arriba".equalsIgnoreCase( dir ) ){
                dir_vertical = 5;
                return;
            }
            dir_vertical = 0;
        }   //fin del metodo setDir_vertical
    }   //fin de la clase MuevePantalla
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Clase MuestraTurno ">
    /**Clase para mostar con un mensaje, quién juego en el turno actual.
     *Aparece "encima" de todo, y sólo se puede ver durante 4 segundos*/
    private class MuestraTurno extends Thread{
        public void run(){
            label_mostrar_turno.setText( "Turno de " +
                    GestorJugadores.getInstancia().jugadorActual().getNombre() );
            label_mostrar_turno.setVisible( true );
            try {
                sleep( 4000 );
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }   //fin del try & catch
            label_mostrar_turno.setVisible( false );
        }   //fin del run
    }   //fin de la clase MuestraTurno
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Clase MuestraCarta ">
    /**Muestra por unos segundos, la carta que se obtuvo del maso.*/
    private class MuestraCarta extends Thread{
        public void run(){
            grafico_carta_tomada.setLocation(
                    (pantalla.width/2 - grafico_carta_tomada.getSize().width/2),
                    (pantalla.height/2 - (grafico_carta_tomada.getSize().height -50) ));
            valoresCartaActual();
            
            grafico_carta_tomada.setVisible( true );
            mostrarValoresCartaActual();
            
            try {
                sleep( 4000 );
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }   //fin del try & catch
            grafico_carta_tomada.setVisible( false );
            ocultarValoresCartaActual();
        }   //fin del metodo run
        
    }   //fin de la clase MuestraCarta
    // </editor-fold>
    
    /**
     *  Instancia un nuevo hilo para mover a un coco dentro  del tablero.
     *
     *  @param: El numero del jugador.
     *  Si se quiere mover al jugador actual, se le envia el numero del  jugador
     *  actual. En caso contrario, se le envia el numero del jugador dos.
     */
    private class MoverCoco2 extends Thread{
        
        private Coco c;
        private int pos_restantes;
        private int jugador;
        
        /**
         *  Instancia un nuevo hilo para mover a un coco dentro  del tablero.
         *
         *  @param: El numero del jugador.
         *  Si se quiere mover al jugador actual, se le envia el numero del  jugador
         *  actual. En caso contrario, se le envia el numero del jugador dos.
         */
        public MoverCoco2(int j){
            jugador = j;
            if( j == GestorJugadores.getInstancia().jugadorActual().getNumero_jugador() ){
                c = GestorJugadores.getInstancia().jugadorActual().getCoco();
                pos_restantes = c.getEspecialidad().mover(carta_actual.getMovimiento());
            }else{
                c = GestorJugadores.getInstancia().jugadorDos().getCoco();
                pos_restantes = c.getEspecialidad().mover(carta_actual.getMovimiento());
            }
        }   //fin del constructor
        
        public void run(){
            tomo_carta = true;  //para evitar que se tome otra carta
            grafico_carta.setIcon( new ImageIcon("src/imagenes/tarjetas/BackCard Gris.jpg" ));
            int pos;    //para asignar el grafico correspondiente
            boolean negativo = false;
            if (pos_restantes < 0){ //analiza si hay movimiento hacia atrás
                pos_restantes *= -1;
                negativo = true;
            }   //fin del if
            while( pos_restantes > 0 ){
                
                if (negativo){  //retroceder
                    c.disminuirPos();
                    pos = GestorJugadores.getInstancia().jugadorActual().getCoco().getPosicion() - 1;
                    if( c.getPosicion() < 0 ){  //analiza si ya no deberia avanzar mas, pues esta en la poscion 0
                        try {
                            sleep(5000);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        GestorJugadores.getInstancia().jugadorActual().getCoco().setPosicion(0);
                        cambiarTurno();
                        return;
                    }   //fin del if
                }else{  //avanzar
                    c.aumentarPos();
                    pos = GestorJugadores.getInstancia().jugadorActual().getCoco().getPosicion() + 1;
                }   //fin del if ... else
                
                /*Decide cual grafico debe mover*/
                switch( jugador ){
                    case 1: grafico_coco1.setVisible(false);
                    grafico_coco1.setIcon( new ImageIcon(
                            graficoParaMovimiento(pos,c)));
                    grafico_coco1.setVisible(true);
                    grafico_coco1.setLocation(
                            dePosicionACasilla(c.getPosicion()));
                    break;
                    case 2: grafico_coco2.setVisible(false);
                    grafico_coco2.setIcon( new ImageIcon(
                            graficoParaMovimiento(pos,c)));
                    grafico_coco2.setVisible(true);
                    grafico_coco2.setLocation(
                            dePosicionACasilla(c.getPosicion()).x,
                            dePosicionACasilla(c.getPosicion()).y +
                            grafico_coco1.getSize().height + 2);
                }   //fin del switch
                pos_restantes--;    //disminuye la cantidad de posiciones que le quedan por avanzar
                try {
                    sleep(100); //se espera para que se vea que se mueve de casilla en casilla
                    new AePlayWave( "src/musica/efectos/pasos.wav").start();
                } catch (InterruptedException ex) {}
            }   //fin del while pos_restantes
            /*Una vez que se ha movido, no queda nada mas que este jugador pueda hacer*/
            try {
                sleep(5000);    //da tiempo para que todo en la pantalla se limpie
                //(porque puede que algo se este desplegando. Ej. una carta.
            } catch (InterruptedException ex) {
            }finally{
                analizadorDeVictoria(); //para ver si se ha llegado a una condicion de victoria
                if( !gano ){
                    cambiarTurno(); //cambia el turno
                }   //fin del if
            }   //fin del try/catch/finally
        }   //fin del metodo run
    }   //fin
    
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        capa_fondo = new javax.swing.JLayeredPane();
        capa_tablero = new javax.swing.JLayeredPane();
        label_fondo = new javax.swing.JLabel();
        grafico_coco2 = new javax.swing.JLabel();
        grafico_coco1 = new javax.swing.JLabel();
        panel_salida = new javax.swing.JPanel();
        casilla1 = new javax.swing.JPanel();
        casilla2 = new javax.swing.JPanel();
        casilla3 = new javax.swing.JPanel();
        casilla4 = new javax.swing.JPanel();
        casilla5 = new javax.swing.JPanel();
        casilla6 = new javax.swing.JPanel();
        casilla7 = new javax.swing.JPanel();
        casilla8 = new javax.swing.JPanel();
        casilla9 = new javax.swing.JPanel();
        casilla10 = new javax.swing.JPanel();
        casilla11 = new javax.swing.JPanel();
        casilla12 = new javax.swing.JPanel();
        casilla13 = new javax.swing.JPanel();
        casilla14 = new javax.swing.JPanel();
        casilla15 = new javax.swing.JPanel();
        casilla16 = new javax.swing.JPanel();
        casilla17 = new javax.swing.JPanel();
        casilla18 = new javax.swing.JPanel();
        casilla19 = new javax.swing.JPanel();
        casilla20 = new javax.swing.JPanel();
        casilla21 = new javax.swing.JPanel();
        casilla22 = new javax.swing.JPanel();
        casilla23 = new javax.swing.JPanel();
        casilla24 = new javax.swing.JPanel();
        casilla25 = new javax.swing.JPanel();
        casilla26 = new javax.swing.JPanel();
        casilla27 = new javax.swing.JPanel();
        label_mostrar_turno = new javax.swing.JLabel();
        label_puntaje_carta_defensa = new javax.swing.JLabel();
        label_poder_carta_defensa = new javax.swing.JLabel();
        label_poder = new javax.swing.JLabel();
        label_puntaje = new javax.swing.JLabel();
        label_movimiento = new javax.swing.JLabel();
        grafico_carta_tomada = new javax.swing.JLabel();
        panel_menu = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        grafico_carta = new javax.swing.JLabel();
        btn_salir = new javax.swing.JButton();
        panel_jugador = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        label_jugador = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        label_nombre_coco = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        label_puntos = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        label_especialidad = new javax.swing.JLabel();
        retrato_coco = new javax.swing.JLabel();
        panel_guerra = new javax.swing.JPanel();
        grafico_carta_mala = new javax.swing.JLabel();
        grafico_carta_buena = new javax.swing.JLabel();
        btn_pelear = new javax.swing.JButton();
        btn_sig_carta_defensa = new javax.swing.JButton();
        btn_ant_carta_defensa = new javax.swing.JButton();
        panel_carta_defensa = new javax.swing.JPanel();
        grafico_carta_defensa = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btn_si = new javax.swing.JButton();
        btn_no = new javax.swing.JButton();
        txt_mensaje_error_carta_defensa = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Coco Void");
        setResizable(false);
        setUndecorated(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        capa_fondo.setFocusable(false);
        capa_fondo.setPreferredSize(new java.awt.Dimension(2000, 2000));
        capa_fondo.setRequestFocusEnabled(false);
        capa_fondo.setVerifyInputWhenFocusTarget(false);
        label_fondo.setBounds(0, 0, 1700, 1700);
        capa_tablero.add(label_fondo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        grafico_coco2.setForeground(new java.awt.Color(255, 255, 255));
        grafico_coco2.setBounds(80, 1560, 180, 110);
        capa_tablero.add(grafico_coco2, javax.swing.JLayeredPane.DRAG_LAYER);

        grafico_coco1.setForeground(new java.awt.Color(255, 255, 255));
        grafico_coco1.setBounds(80, 1460, 180, 110);
        capa_tablero.add(grafico_coco1, javax.swing.JLayeredPane.DRAG_LAYER);

        panel_salida.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        panel_salida.setFocusable(false);
        panel_salida.setOpaque(false);
        panel_salida.setRequestFocusEnabled(false);
        panel_salida.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout panel_salidaLayout = new org.jdesktop.layout.GroupLayout(panel_salida);
        panel_salida.setLayout(panel_salidaLayout);
        panel_salidaLayout.setHorizontalGroup(
            panel_salidaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        panel_salidaLayout.setVerticalGroup(
            panel_salidaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        panel_salida.setBounds(70, 1450, 200, 230);
        capa_tablero.add(panel_salida, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla1.setFocusable(false);
        casilla1.setOpaque(false);
        casilla1.setRequestFocusEnabled(false);
        casilla1.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla1Layout = new org.jdesktop.layout.GroupLayout(casilla1);
        casilla1.setLayout(casilla1Layout);
        casilla1Layout.setHorizontalGroup(
            casilla1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla1Layout.setVerticalGroup(
            casilla1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla1.setBounds(270, 1450, 200, 230);
        capa_tablero.add(casilla1, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla2.setFocusable(false);
        casilla2.setOpaque(false);
        casilla2.setRequestFocusEnabled(false);
        casilla2.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla2Layout = new org.jdesktop.layout.GroupLayout(casilla2);
        casilla2.setLayout(casilla2Layout);
        casilla2Layout.setHorizontalGroup(
            casilla2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla2Layout.setVerticalGroup(
            casilla2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla2.setBounds(470, 1450, 200, 230);
        capa_tablero.add(casilla2, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla3.setFocusable(false);
        casilla3.setOpaque(false);
        casilla3.setRequestFocusEnabled(false);
        casilla3.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla3Layout = new org.jdesktop.layout.GroupLayout(casilla3);
        casilla3.setLayout(casilla3Layout);
        casilla3Layout.setHorizontalGroup(
            casilla3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla3Layout.setVerticalGroup(
            casilla3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla3.setBounds(670, 1450, 200, 230);
        capa_tablero.add(casilla3, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla4.setFocusable(false);
        casilla4.setOpaque(false);
        casilla4.setRequestFocusEnabled(false);
        casilla4.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla4Layout = new org.jdesktop.layout.GroupLayout(casilla4);
        casilla4.setLayout(casilla4Layout);
        casilla4Layout.setHorizontalGroup(
            casilla4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla4Layout.setVerticalGroup(
            casilla4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla4.setBounds(870, 1450, 200, 230);
        capa_tablero.add(casilla4, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla5.setFocusable(false);
        casilla5.setOpaque(false);
        casilla5.setRequestFocusEnabled(false);
        casilla5.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla5Layout = new org.jdesktop.layout.GroupLayout(casilla5);
        casilla5.setLayout(casilla5Layout);
        casilla5Layout.setHorizontalGroup(
            casilla5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla5Layout.setVerticalGroup(
            casilla5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla5.setBounds(1070, 1450, 200, 230);
        capa_tablero.add(casilla5, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla6.setFocusable(false);
        casilla6.setOpaque(false);
        casilla6.setRequestFocusEnabled(false);
        casilla6.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla6Layout = new org.jdesktop.layout.GroupLayout(casilla6);
        casilla6.setLayout(casilla6Layout);
        casilla6Layout.setHorizontalGroup(
            casilla6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla6Layout.setVerticalGroup(
            casilla6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla6.setBounds(1270, 1450, 200, 230);
        capa_tablero.add(casilla6, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla7.setFocusable(false);
        casilla7.setOpaque(false);
        casilla7.setRequestFocusEnabled(false);
        casilla7.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla7Layout = new org.jdesktop.layout.GroupLayout(casilla7);
        casilla7.setLayout(casilla7Layout);
        casilla7Layout.setHorizontalGroup(
            casilla7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla7Layout.setVerticalGroup(
            casilla7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla7.setBounds(1470, 1450, 200, 230);
        capa_tablero.add(casilla7, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla8.setFocusable(false);
        casilla8.setOpaque(false);
        casilla8.setRequestFocusEnabled(false);
        casilla8.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla8Layout = new org.jdesktop.layout.GroupLayout(casilla8);
        casilla8.setLayout(casilla8Layout);
        casilla8Layout.setHorizontalGroup(
            casilla8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla8Layout.setVerticalGroup(
            casilla8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla8.setBounds(1470, 1220, 200, 230);
        capa_tablero.add(casilla8, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla9.setFocusable(false);
        casilla9.setOpaque(false);
        casilla9.setRequestFocusEnabled(false);
        casilla9.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla9Layout = new org.jdesktop.layout.GroupLayout(casilla9);
        casilla9.setLayout(casilla9Layout);
        casilla9Layout.setHorizontalGroup(
            casilla9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla9Layout.setVerticalGroup(
            casilla9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla9.setBounds(1470, 990, 200, 230);
        capa_tablero.add(casilla9, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla10.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla10.setFocusable(false);
        casilla10.setOpaque(false);
        casilla10.setRequestFocusEnabled(false);
        casilla10.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla10Layout = new org.jdesktop.layout.GroupLayout(casilla10);
        casilla10.setLayout(casilla10Layout);
        casilla10Layout.setHorizontalGroup(
            casilla10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla10Layout.setVerticalGroup(
            casilla10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla10.setBounds(1470, 760, 200, 230);
        capa_tablero.add(casilla10, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla11.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla11.setFocusable(false);
        casilla11.setOpaque(false);
        casilla11.setRequestFocusEnabled(false);
        casilla11.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla11Layout = new org.jdesktop.layout.GroupLayout(casilla11);
        casilla11.setLayout(casilla11Layout);
        casilla11Layout.setHorizontalGroup(
            casilla11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla11Layout.setVerticalGroup(
            casilla11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla11.setBounds(1270, 760, 200, 230);
        capa_tablero.add(casilla11, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla12.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla12.setFocusable(false);
        casilla12.setOpaque(false);
        casilla12.setRequestFocusEnabled(false);
        casilla12.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla12Layout = new org.jdesktop.layout.GroupLayout(casilla12);
        casilla12.setLayout(casilla12Layout);
        casilla12Layout.setHorizontalGroup(
            casilla12Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla12Layout.setVerticalGroup(
            casilla12Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla12.setBounds(1070, 760, 200, 230);
        capa_tablero.add(casilla12, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla13.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla13.setFocusable(false);
        casilla13.setOpaque(false);
        casilla13.setRequestFocusEnabled(false);
        casilla13.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla13Layout = new org.jdesktop.layout.GroupLayout(casilla13);
        casilla13.setLayout(casilla13Layout);
        casilla13Layout.setHorizontalGroup(
            casilla13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla13Layout.setVerticalGroup(
            casilla13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla13.setBounds(870, 760, 200, 230);
        capa_tablero.add(casilla13, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla14.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla14.setFocusable(false);
        casilla14.setOpaque(false);
        casilla14.setRequestFocusEnabled(false);
        casilla14.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla14Layout = new org.jdesktop.layout.GroupLayout(casilla14);
        casilla14.setLayout(casilla14Layout);
        casilla14Layout.setHorizontalGroup(
            casilla14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla14Layout.setVerticalGroup(
            casilla14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla14.setBounds(670, 760, 200, 230);
        capa_tablero.add(casilla14, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla15.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla15.setFocusable(false);
        casilla15.setOpaque(false);
        casilla15.setRequestFocusEnabled(false);
        casilla15.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla15Layout = new org.jdesktop.layout.GroupLayout(casilla15);
        casilla15.setLayout(casilla15Layout);
        casilla15Layout.setHorizontalGroup(
            casilla15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla15Layout.setVerticalGroup(
            casilla15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla15.setBounds(470, 760, 200, 230);
        capa_tablero.add(casilla15, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla16.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla16.setFocusable(false);
        casilla16.setOpaque(false);
        casilla16.setRequestFocusEnabled(false);
        casilla16.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla16Layout = new org.jdesktop.layout.GroupLayout(casilla16);
        casilla16.setLayout(casilla16Layout);
        casilla16Layout.setHorizontalGroup(
            casilla16Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla16Layout.setVerticalGroup(
            casilla16Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla16.setBounds(270, 760, 200, 230);
        capa_tablero.add(casilla16, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla17.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla17.setFocusable(false);
        casilla17.setOpaque(false);
        casilla17.setRequestFocusEnabled(false);
        casilla17.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla17Layout = new org.jdesktop.layout.GroupLayout(casilla17);
        casilla17.setLayout(casilla17Layout);
        casilla17Layout.setHorizontalGroup(
            casilla17Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla17Layout.setVerticalGroup(
            casilla17Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla17.setBounds(70, 760, 200, 230);
        capa_tablero.add(casilla17, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla18.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla18.setFocusable(false);
        casilla18.setOpaque(false);
        casilla18.setRequestFocusEnabled(false);
        casilla18.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla18Layout = new org.jdesktop.layout.GroupLayout(casilla18);
        casilla18.setLayout(casilla18Layout);
        casilla18Layout.setHorizontalGroup(
            casilla18Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla18Layout.setVerticalGroup(
            casilla18Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla18.setBounds(70, 530, 200, 230);
        capa_tablero.add(casilla18, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla19.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla19.setFocusable(false);
        casilla19.setOpaque(false);
        casilla19.setRequestFocusEnabled(false);
        casilla19.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla19Layout = new org.jdesktop.layout.GroupLayout(casilla19);
        casilla19.setLayout(casilla19Layout);
        casilla19Layout.setHorizontalGroup(
            casilla19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla19Layout.setVerticalGroup(
            casilla19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla19.setBounds(70, 300, 200, 230);
        capa_tablero.add(casilla19, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla20.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla20.setFocusable(false);
        casilla20.setOpaque(false);
        casilla20.setRequestFocusEnabled(false);
        casilla20.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla20Layout = new org.jdesktop.layout.GroupLayout(casilla20);
        casilla20.setLayout(casilla20Layout);
        casilla20Layout.setHorizontalGroup(
            casilla20Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla20Layout.setVerticalGroup(
            casilla20Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla20.setBounds(70, 70, 200, 230);
        capa_tablero.add(casilla20, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla21.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla21.setFocusable(false);
        casilla21.setOpaque(false);
        casilla21.setRequestFocusEnabled(false);
        casilla21.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla21Layout = new org.jdesktop.layout.GroupLayout(casilla21);
        casilla21.setLayout(casilla21Layout);
        casilla21Layout.setHorizontalGroup(
            casilla21Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla21Layout.setVerticalGroup(
            casilla21Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla21.setBounds(270, 70, 200, 230);
        capa_tablero.add(casilla21, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla22.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla22.setFocusable(false);
        casilla22.setOpaque(false);
        casilla22.setRequestFocusEnabled(false);
        casilla22.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla22Layout = new org.jdesktop.layout.GroupLayout(casilla22);
        casilla22.setLayout(casilla22Layout);
        casilla22Layout.setHorizontalGroup(
            casilla22Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla22Layout.setVerticalGroup(
            casilla22Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla22.setBounds(470, 70, 200, 230);
        capa_tablero.add(casilla22, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla23.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla23.setFocusable(false);
        casilla23.setOpaque(false);
        casilla23.setRequestFocusEnabled(false);
        casilla23.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla23Layout = new org.jdesktop.layout.GroupLayout(casilla23);
        casilla23.setLayout(casilla23Layout);
        casilla23Layout.setHorizontalGroup(
            casilla23Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla23Layout.setVerticalGroup(
            casilla23Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla23.setBounds(670, 70, 200, 230);
        capa_tablero.add(casilla23, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla24.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla24.setFocusable(false);
        casilla24.setOpaque(false);
        casilla24.setRequestFocusEnabled(false);
        casilla24.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla24Layout = new org.jdesktop.layout.GroupLayout(casilla24);
        casilla24.setLayout(casilla24Layout);
        casilla24Layout.setHorizontalGroup(
            casilla24Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla24Layout.setVerticalGroup(
            casilla24Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla24.setBounds(870, 70, 200, 230);
        capa_tablero.add(casilla24, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla25.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla25.setFocusable(false);
        casilla25.setOpaque(false);
        casilla25.setRequestFocusEnabled(false);
        casilla25.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla25Layout = new org.jdesktop.layout.GroupLayout(casilla25);
        casilla25.setLayout(casilla25Layout);
        casilla25Layout.setHorizontalGroup(
            casilla25Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla25Layout.setVerticalGroup(
            casilla25Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla25.setBounds(1070, 70, 200, 230);
        capa_tablero.add(casilla25, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla26.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla26.setFocusable(false);
        casilla26.setOpaque(false);
        casilla26.setRequestFocusEnabled(false);
        casilla26.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla26Layout = new org.jdesktop.layout.GroupLayout(casilla26);
        casilla26.setLayout(casilla26Layout);
        casilla26Layout.setHorizontalGroup(
            casilla26Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla26Layout.setVerticalGroup(
            casilla26Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla26.setBounds(1270, 70, 200, 230);
        capa_tablero.add(casilla26, javax.swing.JLayeredPane.PALETTE_LAYER);

        casilla27.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        casilla27.setFocusable(false);
        casilla27.setOpaque(false);
        casilla27.setRequestFocusEnabled(false);
        casilla27.setVerifyInputWhenFocusTarget(false);
        org.jdesktop.layout.GroupLayout casilla27Layout = new org.jdesktop.layout.GroupLayout(casilla27);
        casilla27.setLayout(casilla27Layout);
        casilla27Layout.setHorizontalGroup(
            casilla27Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 194, Short.MAX_VALUE)
        );
        casilla27Layout.setVerticalGroup(
            casilla27Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 224, Short.MAX_VALUE)
        );
        casilla27.setBounds(1470, 70, 200, 230);
        capa_tablero.add(casilla27, javax.swing.JLayeredPane.PALETTE_LAYER);

        capa_tablero.setBounds(0, 0, 1700, 1700);
        capa_fondo.add(capa_tablero, javax.swing.JLayeredPane.DEFAULT_LAYER);

        label_mostrar_turno.setFont(new java.awt.Font("Dialog", 1, 72));
        label_mostrar_turno.setForeground(new java.awt.Color(255, 255, 255));
        label_mostrar_turno.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_mostrar_turno.setText("Turno");
        label_mostrar_turno.setBounds(70, 1010, 1024, 140);
        capa_fondo.add(label_mostrar_turno, javax.swing.JLayeredPane.POPUP_LAYER);

        label_puntaje_carta_defensa.setFont(new java.awt.Font("Tahoma", 0, 50));
        label_puntaje_carta_defensa.setForeground(new java.awt.Color(255, 255, 255));
        label_puntaje_carta_defensa.setText("10");
        label_puntaje_carta_defensa.setBounds(290, 320, 64, 59);
        capa_fondo.add(label_puntaje_carta_defensa, javax.swing.JLayeredPane.DRAG_LAYER);

        label_poder_carta_defensa.setFont(new java.awt.Font("Tahoma", 0, 50));
        label_poder_carta_defensa.setForeground(new java.awt.Color(255, 255, 255));
        label_poder_carta_defensa.setText("10");
        label_poder_carta_defensa.setBounds(290, 320, 64, 59);
        capa_fondo.add(label_poder_carta_defensa, javax.swing.JLayeredPane.DRAG_LAYER);

        label_poder.setFont(new java.awt.Font("Tahoma", 0, 50));
        label_poder.setForeground(new java.awt.Color(255, 255, 255));
        label_poder.setText("10");
        label_poder.setBounds(290, 320, 64, 59);
        capa_fondo.add(label_poder, javax.swing.JLayeredPane.DRAG_LAYER);

        label_puntaje.setFont(new java.awt.Font("Tahoma", 0, 50));
        label_puntaje.setForeground(new java.awt.Color(255, 255, 255));
        label_puntaje.setText("10");
        label_puntaje.setBounds(290, 320, 64, 59);
        capa_fondo.add(label_puntaje, javax.swing.JLayeredPane.DRAG_LAYER);

        label_movimiento.setFont(new java.awt.Font("Tahoma", 0, 50));
        label_movimiento.setForeground(new java.awt.Color(255, 255, 255));
        label_movimiento.setText("10");
        label_movimiento.setBounds(290, 320, 64, 59);
        capa_fondo.add(label_movimiento, javax.swing.JLayeredPane.DRAG_LAYER);

        grafico_carta_tomada.setBounds(284, 310, 344, 450);
        capa_fondo.add(grafico_carta_tomada, javax.swing.JLayeredPane.POPUP_LAYER);

        panel_menu.setBackground(new java.awt.Color(102, 102, 102));
        panel_menu.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        jPanel1.setFocusable(false);
        jPanel1.setOpaque(false);
        jPanel1.setRequestFocusEnabled(false);
        jPanel1.setVerifyInputWhenFocusTarget(false);
        grafico_carta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tarjetas/BackCard.jpg")));
        grafico_carta.setToolTipText("De click aqui para tomar una carta");
        grafico_carta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                grafico_cartaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                grafico_cartaMouseEntered(evt);
            }
        });

        btn_salir.setBackground(new java.awt.Color(0, 0, 0));
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });
        btn_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_salirMouseEntered(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(grafico_carta)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_salir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(grafico_carta)
                .addContainerGap(82, Short.MAX_VALUE))
            .add(jPanel1Layout.createSequentialGroup()
                .add(btn_salir)
                .addContainerGap(293, Short.MAX_VALUE))
        );

        panel_jugador.setEnabled(false);
        panel_jugador.setFocusable(false);
        panel_jugador.setOpaque(false);
        panel_jugador.setPreferredSize(new java.awt.Dimension(250, 328));
        panel_jugador.setRequestFocusEnabled(false);
        panel_jugador.setVerifyInputWhenFocusTarget(false);
        jLabel1.setForeground(new java.awt.Color(153, 255, 0));
        jLabel1.setText("Jugador:");

        label_jugador.setFont(new java.awt.Font("Screengem", 1, 24));
        label_jugador.setForeground(new java.awt.Color(255, 255, 255));
        label_jugador.setText("Nombre");

        jLabel2.setForeground(new java.awt.Color(153, 255, 0));
        jLabel2.setText("Coco:");

        label_nombre_coco.setFont(new java.awt.Font("Screengem", 1, 24));
        label_nombre_coco.setForeground(new java.awt.Color(255, 255, 255));
        label_nombre_coco.setText("Coco");

        jLabel3.setForeground(new java.awt.Color(153, 255, 0));
        jLabel3.setText("Puntos:");

        label_puntos.setFont(new java.awt.Font("Screengem", 1, 24));
        label_puntos.setForeground(new java.awt.Color(255, 255, 255));
        label_puntos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_puntos.setText("0");

        jLabel5.setForeground(new java.awt.Color(153, 255, 0));
        jLabel5.setText("Especialidad del coco:");

        label_especialidad.setForeground(new java.awt.Color(255, 255, 255));
        label_especialidad.setText("Ninguna");

        org.jdesktop.layout.GroupLayout panel_jugadorLayout = new org.jdesktop.layout.GroupLayout(panel_jugador);
        panel_jugador.setLayout(panel_jugadorLayout);
        panel_jugadorLayout.setHorizontalGroup(
            panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_jugadorLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel2)
                    .add(label_nombre_coco)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_jugadorLayout.createSequentialGroup()
                        .add(panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel1)
                            .add(label_jugador))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 102, Short.MAX_VALUE)
                        .add(panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(label_puntos, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .add(jLabel5)
                    .add(label_especialidad))
                .addContainerGap())
        );
        panel_jugadorLayout.setVerticalGroup(
            panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_jugadorLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jLabel3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_jugadorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(label_jugador)
                    .add(label_puntos))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(label_nombre_coco)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel5)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(label_especialidad)
                .add(158, 158, 158))
        );

        retrato_coco.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        retrato_coco.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));

        org.jdesktop.layout.GroupLayout panel_menuLayout = new org.jdesktop.layout.GroupLayout(panel_menu);
        panel_menu.setLayout(panel_menuLayout);
        panel_menuLayout.setHorizontalGroup(
            panel_menuLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_menuLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_jugador, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 274, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(retrato_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 250, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(946, 946, 946))
        );
        panel_menuLayout.setVerticalGroup(
            panel_menuLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_menuLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_menuLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(panel_jugador, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 318, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(retrato_coco, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 234, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        panel_menu.setBounds(960, 1070, 400, 150);
        capa_fondo.add(panel_menu, javax.swing.JLayeredPane.DRAG_LAYER);

        panel_guerra.setBackground(new java.awt.Color(102, 102, 102));
        panel_guerra.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        panel_guerra.setFocusable(false);
        panel_guerra.setRequestFocusEnabled(false);
        panel_guerra.setVerifyInputWhenFocusTarget(false);

        btn_pelear.setBackground(new java.awt.Color(0, 0, 0));
        btn_pelear.setForeground(new java.awt.Color(153, 255, 0));
        btn_pelear.setText("\u00a1Pelear!");
        btn_pelear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_pelearActionPerformed(evt);
            }
        });

        btn_sig_carta_defensa.setBackground(new java.awt.Color(0, 0, 0));
        btn_sig_carta_defensa.setForeground(new java.awt.Color(255, 255, 255));
        btn_sig_carta_defensa.setText(">");
        btn_sig_carta_defensa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sig_carta_defensaActionPerformed(evt);
            }
        });

        btn_ant_carta_defensa.setBackground(new java.awt.Color(0, 0, 0));
        btn_ant_carta_defensa.setForeground(new java.awt.Color(255, 255, 255));
        btn_ant_carta_defensa.setText("<");
        btn_ant_carta_defensa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ant_carta_defensaActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panel_guerraLayout = new org.jdesktop.layout.GroupLayout(panel_guerra);
        panel_guerra.setLayout(panel_guerraLayout);
        panel_guerraLayout.setHorizontalGroup(
            panel_guerraLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_guerraLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_guerraLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panel_guerraLayout.createSequentialGroup()
                        .add(grafico_carta_mala, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 344, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 22, Short.MAX_VALUE)
                        .add(grafico_carta_buena, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 344, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(btn_pelear, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panel_guerraLayout.createSequentialGroup()
                        .add(btn_ant_carta_defensa)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btn_sig_carta_defensa)))
                .addContainerGap())
        );
        panel_guerraLayout.setVerticalGroup(
            panel_guerraLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_guerraLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_guerraLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(grafico_carta_mala, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 450, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(grafico_carta_buena, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 450, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(8, 8, 8)
                .add(panel_guerraLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btn_sig_carta_defensa)
                    .add(btn_ant_carta_defensa))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_pelear)
                .addContainerGap(68, Short.MAX_VALUE))
        );
        panel_guerra.setBounds(1140, 340, 740, 600);
        capa_fondo.add(panel_guerra, javax.swing.JLayeredPane.POPUP_LAYER);

        panel_carta_defensa.setBackground(new java.awt.Color(102, 102, 102));
        panel_carta_defensa.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 0), new java.awt.Color(204, 255, 0), new java.awt.Color(153, 255, 0), new java.awt.Color(153, 255, 0)));
        panel_carta_defensa.setFocusable(false);
        panel_carta_defensa.setRequestFocusEnabled(false);
        panel_carta_defensa.setVerifyInputWhenFocusTarget(false);

        jLabel4.setForeground(new java.awt.Color(153, 255, 0));
        jLabel4.setText("\u00bfDesea guardar esta carta de defensa?");

        btn_si.setBackground(new java.awt.Color(0, 0, 0));
        btn_si.setForeground(new java.awt.Color(255, 255, 255));
        btn_si.setText("SI");
        btn_si.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_siActionPerformed(evt);
            }
        });

        btn_no.setBackground(new java.awt.Color(0, 0, 0));
        btn_no.setForeground(new java.awt.Color(255, 255, 255));
        btn_no.setText("NO");
        btn_no.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_noActionPerformed(evt);
            }
        });

        txt_mensaje_error_carta_defensa.setEditable(false);
        txt_mensaje_error_carta_defensa.setForeground(new java.awt.Color(255, 51, 51));
        txt_mensaje_error_carta_defensa.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_mensaje_error_carta_defensa.setBorder(null);
        txt_mensaje_error_carta_defensa.setOpaque(false);

        org.jdesktop.layout.GroupLayout panel_carta_defensaLayout = new org.jdesktop.layout.GroupLayout(panel_carta_defensa);
        panel_carta_defensa.setLayout(panel_carta_defensaLayout);
        panel_carta_defensaLayout.setHorizontalGroup(
            panel_carta_defensaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_carta_defensaLayout.createSequentialGroup()
                .addContainerGap()
                .add(grafico_carta_defensa, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 344, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panel_carta_defensaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel4)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, panel_carta_defensaLayout.createSequentialGroup()
                        .add(btn_si)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 140, Short.MAX_VALUE)
                        .add(btn_no))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txt_mensaje_error_carta_defensa, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE))
                .addContainerGap())
        );
        panel_carta_defensaLayout.setVerticalGroup(
            panel_carta_defensaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panel_carta_defensaLayout.createSequentialGroup()
                .addContainerGap()
                .add(panel_carta_defensaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panel_carta_defensaLayout.createSequentialGroup()
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(panel_carta_defensaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(btn_si)
                            .add(btn_no))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txt_mensaje_error_carta_defensa, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(grafico_carta_defensa, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 450, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel_carta_defensa.setBounds(1150, 440, 620, 470);
        capa_fondo.add(panel_carta_defensa, javax.swing.JLayeredPane.POPUP_LAYER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(capa_fondo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 2000, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(capa_fondo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 2000, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     *  Despliega la siguiente carta de defensa a la hora do combatir con una
     *  carta de retroceso
     */
    private void btn_ant_carta_defensaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ant_carta_defensaActionPerformed
        ocultarValoresGuerra();
        temporal = GestorJugadores.getInstancia().jugadorActual().getCoco().verCarta();
        valoresPanelGuerra();
        mostrarValoresGuerra();
    }//GEN-LAST:event_btn_ant_carta_defensaActionPerformed
    
    /**
     *  Despliega la siguiente carta de defensa a la hora do combatir con una
     *  carta de retroceso
     */
    private void btn_sig_carta_defensaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sig_carta_defensaActionPerformed
        ocultarValoresGuerra();
        temporal = GestorJugadores.getInstancia().jugadorActual().getCoco().verCarta();
        valoresPanelGuerra();
        mostrarValoresGuerra();
    }//GEN-LAST:event_btn_sig_carta_defensaActionPerformed
    
    /**
     *  Analiza la carta de defensa contra la carta de retroceso.
     *  Si la carta de defensa es mayor, no pasa nada.
     *  Si la carta de retroceso es mayor, se mueve al coco
     */
    private void btn_pelearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_pelearActionPerformed
        if( GestorCartas.getInstancia().compararCartas(temporal, carta_actual) ){
            /*No hay que mover al coco*/
            new AePlayWave( "src/musica/efectos/gana_batalla.wav").start();
            GestorJugadores.getInstancia().jugadorActual().getCoco().usarCartaDefensa(temporal);
            panel_guerra.setVisible( false );
            ocultarValoresGuerra();
            cambiarTurno();
        }else{
            /*Se mueve al coco*/
            new AePlayWave( "src/musica/efectos/retroceso.wav").start();
            new MoverCoco2(GestorJugadores.getInstancia().jugadorActual().getNumero_jugador()).start();
            GestorJugadores.getInstancia().jugadorActual().getCoco().usarCartaDefensa(temporal);
            panel_guerra.setVisible( false );
            ocultarValoresGuerra();
        }   //fin del if ... else
    }//GEN-LAST:event_btn_pelearActionPerformed
    
    /**
     *  Guarda la carta de defensa en la lista lista del jugador actual
     */
    private void btn_siActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_siActionPerformed
        try {
            GestorJugadores.getInstancia().jugadorActual().getCoco().agregarCarta( (CartaDefensa) carta_actual);
            new AePlayWave( "src/musica/efectos/gana_batalla.wav").start();
            panel_carta_defensa.setVisible( false );
            ocultarValoresCartaActual();
            cambiarTurno();
        } catch (Exception ex) {
            txt_mensaje_error_carta_defensa.setText( "Usted ya tiene 3 cartas");
        }   //fin del try & catch--
    }//GEN-LAST:event_btn_siActionPerformed
    
    /**
     *  Ignora a la carta de defensa; no la guarda
     */
    private void btn_noActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_noActionPerformed
        new AePlayWave( "src/musica/efectos/retroceso.wav").start();
        panel_carta_defensa.setVisible( false );
        ocultarValoresCartaActual();
        cambiarTurno();
    }//GEN-LAST:event_btn_noActionPerformed
    
    /**Obtiene la primera carta del maso y la muestra. Luego realiza la accion
     *correspondiente a esa carta*/
    private void grafico_cartaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_grafico_cartaMouseClicked
        tomarCarta();
    }//GEN-LAST:event_grafico_cartaMouseClicked
    
    private void grafico_cartaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_grafico_cartaMouseEntered
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_grafico_cartaMouseEntered
    
    private void btn_salirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_salirMouseEntered
        new AePlayWave( "src/musica/efectos/boton.wav" ).start();
    }//GEN-LAST:event_btn_salirMouseEntered
    
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        initFullScreen();
        initGraficosCocos();
        initTurno();
    }//GEN-LAST:event_formWindowOpened
    
    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        new AePlayWave( "src/musica/efectos/click.wav" ).start();
        System.exit( 0 );
    }//GEN-LAST:event_btn_salirActionPerformed
    
    // <editor-fold defaultstate="collapsed" desc=" Metodos para mover pantalla ">
    /**Metodo que analiza la posicion del mouse y decide si se debe mover el
     *mapa o no.*/
    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
        if( evt.getX() <= 30 ){
            centinela = true;
            mp.setDir_horizontal( "izquierda" );
            return;
        }
        if( evt.getY() <= 30 ){
            centinela = true;
            mp.setDir_vertical( "arriba");
            return;
        }
        if( evt.getX() >= (pantalla.width - 30) ){
            centinela = true;
            mp.setDir_horizontal( "derecha" );
            return;
        }
        if( evt.getY() >= (pantalla.height - 30) ){
            centinela = true;
            mp.setDir_vertical( "abajo");
            return;
        }
        centinela = false;
        if( !gano ){
            mp.setDir_horizontal("");
            mp.setDir_vertical( "" );
        }
        
    }//GEN-LAST:event_formMouseMoved
    // </editor-fold>
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ant_carta_defensa;
    private javax.swing.JButton btn_no;
    private javax.swing.JButton btn_pelear;
    private javax.swing.JButton btn_salir;
    private javax.swing.JButton btn_si;
    private javax.swing.JButton btn_sig_carta_defensa;
    private javax.swing.JLayeredPane capa_fondo;
    private javax.swing.JLayeredPane capa_tablero;
    private javax.swing.JPanel casilla1;
    private javax.swing.JPanel casilla10;
    private javax.swing.JPanel casilla11;
    private javax.swing.JPanel casilla12;
    private javax.swing.JPanel casilla13;
    private javax.swing.JPanel casilla14;
    private javax.swing.JPanel casilla15;
    private javax.swing.JPanel casilla16;
    private javax.swing.JPanel casilla17;
    private javax.swing.JPanel casilla18;
    private javax.swing.JPanel casilla19;
    private javax.swing.JPanel casilla2;
    private javax.swing.JPanel casilla20;
    private javax.swing.JPanel casilla21;
    private javax.swing.JPanel casilla22;
    private javax.swing.JPanel casilla23;
    private javax.swing.JPanel casilla24;
    private javax.swing.JPanel casilla25;
    private javax.swing.JPanel casilla26;
    private javax.swing.JPanel casilla27;
    private javax.swing.JPanel casilla3;
    private javax.swing.JPanel casilla4;
    private javax.swing.JPanel casilla5;
    private javax.swing.JPanel casilla6;
    private javax.swing.JPanel casilla7;
    private javax.swing.JPanel casilla8;
    private javax.swing.JPanel casilla9;
    private javax.swing.JLabel grafico_carta;
    private javax.swing.JLabel grafico_carta_buena;
    private javax.swing.JLabel grafico_carta_defensa;
    private javax.swing.JLabel grafico_carta_mala;
    private javax.swing.JLabel grafico_carta_tomada;
    private javax.swing.JLabel grafico_coco1;
    private javax.swing.JLabel grafico_coco2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel label_especialidad;
    private javax.swing.JLabel label_fondo;
    private javax.swing.JLabel label_jugador;
    private javax.swing.JLabel label_mostrar_turno;
    private javax.swing.JLabel label_movimiento;
    private javax.swing.JLabel label_nombre_coco;
    private javax.swing.JLabel label_poder;
    private javax.swing.JLabel label_poder_carta_defensa;
    private javax.swing.JLabel label_puntaje;
    private javax.swing.JLabel label_puntaje_carta_defensa;
    private javax.swing.JLabel label_puntos;
    private javax.swing.JPanel panel_carta_defensa;
    private javax.swing.JPanel panel_guerra;
    private javax.swing.JPanel panel_jugador;
    private javax.swing.JPanel panel_menu;
    private javax.swing.JPanel panel_salida;
    private javax.swing.JLabel retrato_coco;
    private javax.swing.JTextField txt_mensaje_error_carta_defensa;
    // End of variables declaration//GEN-END:variables
    private boolean centinela, tomo_carta;
    private Dimension pantalla;
    private Dimension mapa;
    private MuevePantalla mp;
    private GraphicsDevice gd;
    private BufferStrategy bufferStrategy;
    private int pWidth, pHeight;
    private PlayWaveLoop musica, musica2;
    private Carta carta_actual;
    private CartaDefensa temporal;
    private boolean gano;
}   //fin de la clase VentanaTablero