package clases;

import clases.Carta.CartaDefensa;
import clases.otros.Coco;
import clases.otros.Jugador;
import java.util.ArrayList;

public class GestorJugadores {
    
    private static GestorJugadores instancia;
    private ArrayList jugadores;    //Mantiene la lista de los jugadores jugando
    private Jugador ganador;
    
    private GestorJugadores() {
        jugadores = new ArrayList();
    }
    
    /**Agrega un nuevo jugador (instancia de la clase Jugador) a la lista de jugadores*/
    public void agregarJugador( Jugador j ){
        jugadores.add( j );
    }   //fin del metodo agregarJugador
    
    /**Obtiene el jugador que se encuentra en el turno actual
     @returns: Jugador*/
    public Jugador jugadorActual(){
        return ((Jugador)jugadores.get(0));
    }   //fin del metodo jugadorActual
    
    public Jugador jugadorDos(){
        return ((Jugador)jugadores.get(1));
    }   //fin del metodo jugadorDos
    
    public boolean estaVaciaJugadorUno(){
        return jugadorActual().getCoco().estaVacio();
    }   //fin del metodo estaVaciaJugadorUno
    
    public boolean estaVaciaJugadorDos(){
        return jugadorDos().getCoco().estaVacio();
    }   //fin del metodo estaVaciaJugadorDos
    
    /**Sirve para que, al cambiar de turno, se cambie al jugador actual*/
    public void cambiarTurno(){
        Jugador temp = jugadorActual();
        jugadores.remove( temp );
        jugadores.add( temp );
        temp = null;
    }   //fin del metodo cambiarTurno
    
    /**Agrega una carta de defensa a la lista de cartas de defensa del jugador actual.
     *@throws: Exception, cuando la lista de cartas de defensa del jugador actual
     *es igual a 3. Esto quiere decir que la cantidad maxima de cartas en la lista
     *de cartas de defensa para cualquier jugador, es de 3.*/
    public void agregarCarta( CartaDefensa cd ) throws Exception{
        jugadorActual().getCoco().agregarCarta( cd );
    }   //fin del metodo agregarCarta
    
    /**Usa una carta de defensa para contrarestar el efecto de una carta negativa.
     @parameters: CartaDefensa.
     *@returns: CartaDefensa.*/
    public void usarCartaDefensa( CartaDefensa cd ){
        jugadorActual().getCoco().usarCartaDefensa( cd );
    }   //fin del metodo usarCartaDefensa
    
    /**Metodo del Singleton. Obtinene el objeto instancia de la clase.
     *@returns: GestorJugadores*/
    public static GestorJugadores getInstancia() {
        if( instancia != null ){
            return instancia;
        }else{
            instancia = new GestorJugadores();
            return instancia;
        }   //fin del if ... else
    }   //fin del metodo getInstancia
    
    /*
     *  Limpia toda la informacion que contenga el gestor
     */
    public void limpiar(){
        jugadores.clear();
        jugadores = null;
        instancia = null;
    }
    
    public Jugador getGanador() {
        return ganador;
    }

    public void setGanador(Jugador ganador) {
        this.ganador = ganador;
    }
    
}   //fin de la clase GestorJugadores