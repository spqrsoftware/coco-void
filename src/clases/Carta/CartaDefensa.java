package clases.Carta;

import java.awt.Point;

public class CartaDefensa extends Carta {
    
    public CartaDefensa() {
        this(0, 0, 0);
    }
    
    public CartaDefensa(int poder, int puntaje, int movimiento) {
        this(poder, puntaje, movimiento, null, null);
    }
    
    public CartaDefensa(String frente, String reverso) {
        this(0, 0, 0, frente, reverso);
    }
    
    public CartaDefensa(int poder, int puntaje, int movimiento, String frente, String reverso) {
        super(poder, puntaje, movimiento, frente, reverso);
        
        Point a = new Point(91, 117);
        Point b = new Point(236, 117);
        Point c = null;
        setPosiciones(a, b, c);
    }
    
    public String toString(){
        String msn = "\nDefensa";
        msn += "\nPuntaje: " + getPuntaje();
        msn += "\nPoder: " + getPoder();
        msn += "\nRetroceso: " + getMovimiento();
        return msn;
    }
    
}
