package clases.Carta;

import java.awt.Point;

public class CartaCastigo extends Carta {
    
    public CartaCastigo() {
        this(0, 0, 0);
    }
    
    public CartaCastigo(int poder, int puntaje, int movimiento) {
        this(poder, puntaje, movimiento, null, null);
    }
    
    public CartaCastigo(String frente, String reverso) {
        this(0, 0, 0, frente, reverso);
    }
    
    public CartaCastigo(int poder, int puntaje, int movimiento, String frente, String reverso) {
        super(poder, puntaje, (movimiento * -1), frente, reverso);
        
        Point a = new Point(70, 90);
        Point b = new Point(235, 90);
        Point c = new Point(153, 200);
        setPosiciones(a, b, c);
    }

    public void setPuntaje(int puntaje) {
        super.setPuntaje(puntaje * -1);
    }

    public void setMovimiento(int movimiento) {
        super.setMovimiento(movimiento * -1);
    }
    
    public String toString(){
        String msn = "\nCastigo";
        msn += "\nPuntaje: " + getPuntaje();
        msn += "\nPoder: " + getPoder();
        msn += "\nRetroceso: " + getMovimiento();
        return msn;
    }
    
}
