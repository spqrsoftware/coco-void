package clases.Carta;

import java.awt.Point;

public class CartaRetroceso extends Carta {
    
    public CartaRetroceso() {
        this(0, 0, 0);
    }
    
    public CartaRetroceso(int poder, int puntaje, int movimiento) {
        this(poder, puntaje, movimiento, null, null);
    }
    
    public CartaRetroceso(String frente, String reverso) {
        this(0, 0, 0, frente, reverso);
    }
    
    public CartaRetroceso(int poder, int puntaje, int movimiento, String frente, String reverso) {
        super(poder, puntaje, (movimiento * -1), frente, reverso);
        
        Point a = new Point(73, 97);
        Point b = new Point(233, 97);
        Point c = new Point(158, 204);
        setPosiciones(a, b, c);
    }

    public void setPuntaje(int puntaje) {
        super.setPuntaje(puntaje * -1);
    }

    public void setMovimiento(int movimiento) {
        super.setMovimiento(movimiento * -1);
    }
    
    public String toString(){
        String msn = "\nRetroceso";
        msn += "\nPuntaje: " + getPuntaje();
        msn += "\nPoder: " + getPoder();
        msn += "\nRetroceso: " + getMovimiento();
        return msn;
    }
    
}
