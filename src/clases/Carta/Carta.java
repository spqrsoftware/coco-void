package clases.Carta;

import java.awt.Point;

public class Carta implements java.io.Serializable{
    
    protected int movimiento;
    protected int poder;
    protected int puntaje;
    protected String frente;
    protected String reverso;
    protected Point [] posiciones;
    
    public Carta() {
        this(0, 0, 0);
    }
    
    public Carta(int poder, int puntaje, int movimiento) {
        this(poder, puntaje, movimiento, null, null);
    }
    
    public Carta(String frente, String reverso) {
        this(0, 0, 0, frente, reverso);
    }
    
    public Carta(int poder, int puntaje, int movimiento, String frente, String reverso) {
        setPoder(poder);
        setPuntaje(puntaje);
        setMovimiento(movimiento);
        setFrente(frente);
        setReverso(reverso);
        
        posiciones = new Point [3];
        for (int i = 0; i < posiciones.length; i++) {
            posiciones[i] = new Point();
        }
    }

    /**
     *  Obtiene el valor del poder de la carta.
     *  @return El valor del poder.
     */
    public int getPoder() {
        return poder;
    }

    /**
     *  Modifica el valor del poder de la carta.
     *  @param El valor del poder.
     */
    public void setPoder(int poder) {
        this.poder = poder;
    }

    /**
     *  Obtiene el valor del puntaje de la carta.
     *  @return El valor del puntaje.
     */
    public int getPuntaje() {
        return puntaje;
    }

    /**
     *  Modifica el valor del puntaje de la carta.
     *  @param El valor del puntaje.
     */
    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    /**
     *  Obtiene la direccion en donde es encuentre la imagen del frente de la carta
     *  @return El valor del movimiento.
     */
    public String getFrente() {
        return frente;
    }

    /**
     *  Modifica la direccion en donde es encuentre la imagen del frente de la carta.
     *  @param El valor del puntaje.
     */
    protected void setFrente(String frente) {
        this.frente = frente;
    }

    /**
     *  Obtiene la direccion en donde es encuentre la imagen del reverso de la carta
     *  @return El valor del movimiento.
     */
    public String getReverso() {
        return reverso;
    }

    /**
     *  Modifica la direccion en donde es encuentre la imagen del reverso de la carta.
     *  @param El valor del puntaje.
     */
    protected void setReverso(String reverso) {
        this.reverso = reverso;
    }

    /**
     *  Obtiene el valor del movimiento que el coco va a realizar.
     *  @return El valor del movimiento. Positivo si es avance y negativo si es retroceso
     */
    public int getMovimiento() {
        return movimiento;
    }

    /**
     *  Modifica el valor del movimiento de la carta.
     *  @param El valor del puntaje.
     */
    public void setMovimiento(int movimiento) {
        this.movimiento = movimiento;
    }

    /**
     *  El arreglo de tipo <code>Point</code> que contiene la posicion del
     *  label del valor deseado. 0) Poder, 1) Puntaje, 2) Movimiento. Si la carta
     *  es de defensa la posicion 2) es null.
     *
     *  @return 0) Poder, 1) Puntaje, 2) Movimiento.
     */
    public Point[] getPosiciones() {
        return posiciones;
    }

    /**
     *  El <code>Point</code> que contiene la coordenada donde se debe colocar el
     *  label con el valor deseado. 0) Poder, 1) Puntaje, 2) Movimiento. Si la carta
     *  es de defensa la posicion 2) es null.
     *
     *  @return 0) Poder, 1) Puntaje, 2) Movimiento.
     */
    public Point getPosiciones(int index) {
        return posiciones[index];
    }

    /**
     *  Modifica las coordenadas donde se debe colocar los labels con los valores deseados.
     *
     *  @param a Coordenades del numero con el valor del poder.
     *  @param b Coordenades del numero con el valor del puntaje.
     *  @param c Coordenades del numero con el valor del movimiento.
     */
    protected void setPosiciones(Point a, Point b, Point c) {
        posiciones[0] = a;
        posiciones[1] = b;
        posiciones[2] = c;
    }
}
