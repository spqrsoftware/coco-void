package clases.Carta;

import java.awt.Point;

public class CartaAvance extends Carta {
    
    public CartaAvance() {
        this(0, 0, 0);
    }
    
    public CartaAvance(int poder, int puntaje, int movimiento) {
        this(poder, puntaje, movimiento, null, null);
    }
    
    public CartaAvance(String frente, String reverso) {
        this(0, 0, 0, frente, reverso);
    }
    
    public CartaAvance(int poder, int puntaje, int movimiento, String frente, String reverso) {
        super(poder, puntaje, movimiento, frente, reverso);
        
        Point a = new Point(59, 103);
        Point b = new Point(225, 103);
        Point c = new Point(153, 207);
        setPosiciones(a, b, c);
    }
    
    public String toString(){
        String msn = "\nAvance";
        msn += "\nPuntaje: " + getPuntaje();
        msn += "\nPoder: " + getPoder();
        msn += "\nAvance: " + getMovimiento();
        return msn;
    }
    
}
