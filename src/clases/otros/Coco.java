package clases.otros;

import clases.Carta.CartaDefensa;
import clases.especialidad.Especialidad;
import java.io.Serializable;
import java.util.ArrayList;

public class Coco implements Serializable{
    
    private String nombre;
    private String descripcion;
    private String musica;
    private String imagen;
    private int posicion;
    private int puntaje;
    private Especialidad especialidad;
    private ArrayList cartas_defensa;

//*******************************  constructores  ****************************//
    public Coco() {
        this("sin coco");
    }   //fin del constructor
    
    public Coco( String nombre ){
        this(nombre, "");
    }
    
    public Coco( String nombre, String descripcion ){
        this(nombre, descripcion, "", "");
    }
    
    public Coco(String nombre, String descripcion, String musica, String imagen){
        this(nombre, descripcion, "", "", null);
    }   //fin del constructor
    
    public Coco(String nombre, String descripcion, String musica, String imagen, Especialidad esp){
        setNombre( nombre );
        setDescripcion( descripcion);
        setMusica( musica );
        setImagen( imagen );
        setPosicion( 0 );
        setPuntaje( 0 );
        setEspecialidad( esp );
        cartas_defensa = new ArrayList();
    }   //fin del constructor

//**********************  Metodos sobre las cartas  **************************//
    public void agregarCarta(CartaDefensa cd ) throws Exception {
        if( cartas_defensa.size() == 3 ){
            throw new Exception( "Ya se tienen tres cartas guardadas" );
        }else{
            cartas_defensa.add( cd );
        }   //fin del if ... else
    }   //fin del metodo agregarCarta
    
    public void usarCartaDefensa( CartaDefensa cd ){
        if( cartas_defensa.contains(cd) ){
            cartas_defensa.remove( cd );
        }   //fin del if
    }   //fin del metodo usarCartaDefensa
    
    public boolean estaVacio(){
        if( cartas_defensa.size() == 0 ){
            return true;
        }   //fin del if
        return false;
    }   //fin del metodo estaVacio
    
    /**
     *Metodo que devuelve la primer carta, y reorganiza la lista para que la segunda
     *sea ahora la primera.
     *
     *@return: CartaDefensa
     **/
    public CartaDefensa verCarta(){
        CartaDefensa temp = (CartaDefensa) cartas_defensa.remove(0);
        cartas_defensa.add(temp);
        return temp;
    }   //fin del metodo verCArta
    
//****************************  Sets y gets  *********************************//
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPosicion() {
        return posicion;
    }

    public void aumentarPos(){
        posicion++;
    }   //fin del metodo aumentarPos
    
    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje += puntaje;
    }

    public Especialidad getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad) {
        this.especialidad = especialidad;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String dir_imagen) {
        imagen = dir_imagen;        
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMusica() {
        return musica;
    }

    public void setMusica(String musica) {
        this.musica = musica;
    }

    public void disminuirPos() {
        posicion--;
    }
    
}   //fin de la clase Coco