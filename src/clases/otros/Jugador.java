package clases.otros;

public class Jugador {
    
    private String nombre;
    private Coco coco;
    private int numero_jugador;
    
//******************************  Constructores  *****************************//
    public Jugador() {
        setNombre( "Sin Nombre" );
        setCoco( null );
    }
    
    public Jugador( String n ){
        setNombre( n );
        setCoco( null );
    }   //fin del constructor
    
//*****************************  Sets y gets  ********************************//
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCoco(Coco coco) {
        this.coco = coco;
    }

    public Coco getCoco() {
        return coco;
    }

    public int getNumero_jugador() {
        return numero_jugador;
    }

    public void setNumero_jugador(int numero_jugador) {
        this.numero_jugador = numero_jugador;
    }
    
    public String toString(){
        String txt = "";
        txt += "Jugador: " + getNombre() + "\n";
        txt += "Puntos: " + getCoco().getPuntaje() + "\n";
        txt += "Coco: " + getCoco();
        return txt;
    }   //fin del metodo toString
    
}   //fin de la clase Jugador