package clases;

import clases.Carta.*;
import java.io.*;
import java.util.ArrayList;

/**
 *  Patron Singleton.
 *
 *  Maneja todo los metodos que tienen que ver con las cartas.
 */
public class GestorCartas {
    
    /**
     *  Instancia de la clase
     */
    private static GestorCartas instancia = null;
    
    /**
     *  El mazo de cartas barajadas
     */
    private ArrayList mazo;
    
    /**
     *  Orden:
     *  0) Carta de avance
     *  1) Carta de retroceso
     *  2) Carta de defensa
     *  3) Carta de castigo
     */
    private final float [] PROPORCION = {0.5F, 0.2F, 0.15F, 0.15F};
    
    /**
     *  Localizacion de los archivos
     */
    private static final String ARCHIVO_AVANCE = "src/archivos/cartasAvance.coco";
    private static final String ARCHIVO_RETROCESO = "src/archivos/cartasRetroceso.coco";
    private static final String ARCHIVO_CASTIGO = "src/archivos/cartasCastigo.coco";
    private static final String ARCHIVO_DEFENSA = "src/archivos/cartasDefensa.coco";
    
    /**
     *  Crea una nueva instancia de la clase
     */
    private GestorCartas() {
        mazo = new ArrayList();
    }

    /**
     *  Devuelve la instancia de la clase. 
     *
     *  @return La instancia de esta clase.
     */
    public static GestorCartas getInstancia() {
        if (instancia  == null)
            instancia = new GestorCartas();
        return instancia;
    }

    /**
     *  Devuelve la carta que se encuentre encima del mazo. 
     *
     *  @return La carta que se encuentre encima del mazo, returna 
     *  <code>null</code> si el mazo esta vacio
     */
    public Carta tomarCarta(){
        if (!isVacio()){
            Carta carta = (Carta) mazo.get(0);
            mazo.remove(0);
            return carta;
        }
        return null;
    }

    /**
     *  Verifica si el mazo tiene cartas 
     *
     *  @return <code>true</code> si el mazo esta vacio, <code>false</code>
     *  si el mazo tiene cartas
     */
    public boolean isVacio(){
        if (mazo.size() == 0)
            return true;
        return false;
    }
    
    /**
     *  Compara una carta de defensa contra una carta de castigo
     *  o de retroceso.
     *
     *  @return <code>true</code> Si la carte de defensa ES mas o igual de poderosa.
     *          <code>false</code> si las cartas negativas son mas poderosas.
     */
    public boolean compararCartas(CartaDefensa cd, Carta c ){
        if( cd.getPoder() >= c.getPoder() ){
            return true;
        }   //fin del if
        return false;
    }   //fin del metodo compararCartas
    
    /**
     *  Crea el mazo de cartas que van a ser utilizadas en el juego.
     *  El mazo consta de un 50% de cartas de avance, un 20 % de cartas de retroceso
     *  un 15% de cartas de defensa y un 15% de cartas de castigo. Automaticamente
     *  carga las cartas y las baraja.
     *
     *  @param cantidad La cantidad de cartas que va a tener el mazo
     */
    public void crearMazo(int cantidad){
        ArrayList aux = new ArrayList();
        for (int i = 0; i < PROPORCION.length; i++) {
            int cant = Math.round(PROPORCION[i] * cantidad);
            ArrayList temp = new ArrayList();
            
            switch (i){
                case 0:
                    temp = cargar(ARCHIVO_AVANCE);
                    break;
                case 1:
                    temp = cargar(ARCHIVO_RETROCESO);
                    break;
                case 2:
                    temp = cargar(ARCHIVO_DEFENSA);
                    break;
                case 3:
                    temp = cargar(ARCHIVO_CASTIGO);
                    break;
            }
            
            for (int j = 0; j < cant; j++) {
                int num = temp.size();
                int index = (int) (Math.random() * num);
                Carta carta = (Carta) temp.get(index);
                generarNumeros(carta);
                aux.add(carta);
                temp.remove(index);
            }
        }
        
        barajar(aux);
    }
    
    /**
     *  Baraja el mazo de cartas que recibe 
     *  @param arr Arraylist que contiene las cartas que se quieren barajar
     */
    private void barajar(ArrayList arr){
        while (arr.size() != 0){
            int index = (int) (Math.random() * arr.size());
            mazo.add(arr.get(index));
            arr.remove(index);
        }
    }
    
    /**
     *  El coloca el poder y el puntaje a las cartas. Dependiendo del
     *  tipo de carta que sea tambien le coloca al avance o retroceso.
     *  @param c Carta a la que se le va a generar los valores.
     */
    private void generarNumeros(Carta c){
        int poder = (int) ((Math.random() * 7) + 1);
        c.setPoder(poder);
        
        int puntaje = (int) ((Math.random() * 9) + 1);
        c.setPuntaje(puntaje);
        
        if (!(c instanceof CartaDefensa)){
            int avance = (int) ((Math.random() * 5) + 1);
            c.setMovimiento(avance);
        }
    }
    
    /**
     *  Carga las cartas que se encuentran en el archivo especificado.
     *  @param ARCHIVO String con la direccion del archivo
     *  @return Un ArrayList con las cartas con las cartas contenidas
     *  en el archivo
     */
    private ArrayList cargar(final String ARCHIVO){
        ArrayList t = new ArrayList();
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            fis = new FileInputStream(ARCHIVO);
            ois = new ObjectInputStream(fis);
            t = (ArrayList) ois.readObject();
        } catch (ClassNotFoundException ex) {
            System.err.println("Clase no encontrada (Casting)");
        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado " + ARCHIVO);
        } catch (IOException ex) {
            System.err.println("Error en el archivo");
        }
        return t;
    }
    
    public void limpiar(){
        mazo.clear();
        mazo = null;
        instancia = null;
    }
    
}