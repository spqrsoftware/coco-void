package clases;

import clases.especialidad.*;
import clases.otros.Coco;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.TreeMap;

public class MainCoco {
    
    public MainCoco() {
    }
    
    public static void main(String[] args) {
        TreeMap cocos = new TreeMap();
        TreeMap co;

        Coco c = new Coco("Coco Freeman", "Estudio en el MIT y alcanzo el grado" +
                " de Ph.D en fisica teorica. Su tesis se llamaba Observacion de " +
                "Einstein-Podolsky-Rosen Entanglement en estructuras de supracuantum " +
                "por induccion a travez de cristales transuranicos no lineales con " +
                " un pulso de una Longitud de Onda extremandamente larga (ELW) " +
                "de una fuente de arreglo bloqueado " +
                "(Teletransportacion a travez de materiales muy pesados).",
                "src/musica/Freeman.wav", 
                "src/imagenes/Coco_FREE.gif", new PosibleMovimiento());
        cocos.put(c.getNombre(), c);

        c = new Coco("Coco 70's", "Parece ser que algun experimento realizado por Coco" +
                " Freeman, produjeron patrones de resonancia, dando lugar a una" +
                " flexion del espacio tiempo. Es por esta razon, que este Coco setentero" +
                " tuvo la mala suerte de caer en el bulto de Coco. Ahora, no solo debe luchar" +
                " para salir del bulto de Coco, sino tambien para regresar a su Epoca.",
                "src/musica/Disco.wav", 
                "src/imagenes/Coco_70's.gif", new DefensaDoble());
        cocos.put(c.getNombre(), c);

        c = new Coco("Coco Bombeable", "¡Dele bomba! Se olle decir ahora. La unica mujer" +
                " que fue absorvida por el bulto con espacio infinito. Debe luchar por sobrevivir" +
                " en ese ambiente hostil, donde no encontrara ayuda de nadie, a menos que..." +
                "bueno. Han estado razonando fuera del recipiente.", "src/musica/loadloop.wav", 
                "src/imagenes/Coco_Coca.gif", new TercioCastigo());
        cocos.put(c.getNombre(), c);

        c = new Coco("Coco", "Esto si es de asustarce. El mismisimo Coco, due�o del bulto" +
                " fue tragado por su propio bulto. Aunque es el unico ser que conoce ese" +
                " espacio infinito, ahora lo vera desde otro punto de vista: Desde adentro. "
                , "src/musica/Mono.wav", 
                "src/imagenes/Coco_MEX.gif", new SinEmpecialidad());
        cocos.put(c.getNombre(), c);

        c = new Coco("WarCoco", "Debido a la mision en que se encontraba, que era investigar" +
                " el horizonte de eventos alrededor del bulto de coco, fue tragado accidentalmente." +
                " Sus compa�eros, creyendolo muerto, lo han abandonado a su suerte. WarCoco" +
                " debe ahora usar su entrenamiento militar para sobrevivir al bulto de coco."
                , "src/musica/War.wav", 
                "src/imagenes/Coco_War.gif", new AvanzaDoble());
        cocos.put(c.getNombre(), c);
        
        salvar(cocos);
        co = cargar();
        
        System.err.println(co.containsKey("Coco Freeman"));
    }
    
    private static void salvar(TreeMap coco){
        try{
            ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream("src/archivos/cocos.coco"));
            
            salida.writeObject(coco);
            salida.close();
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }
    
        public static TreeMap cargar(){
        TreeMap t = new TreeMap();
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            fis = new FileInputStream("src/archivos/cocos.coco");
            ois = new ObjectInputStream(fis);
            t = (TreeMap) ois.readObject();
        } catch (ClassNotFoundException ex) {
            System.err.println("Clase no encontrada (Casting)");
        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado");
        } catch (IOException ex) {
            System.err.println("Error en el archivo");
        }
        return (TreeMap) t.clone();
    }
    
}
