package clases.especialidad;

import java.io.Serializable;

public interface Especialidad extends Serializable{
    
    /**
     *  Recibe un entero con la cantidad de casillas a moverse y devuelve
     *  el verdadero movimiento que realiza el coco.
     *  @param valor La cantidad de casillas de avance/retroceso de la carta
     *  @return La cantidad de casillas que avance/retrocede segun la especialidad del coco
     */
    public int mover( int valor );
    
    /**
     *  Recibe un entero con el poder de la carta y devuelve
     *  el verdadero poder que realiza el coco.
     *  @param valor La cantidad de casillas de avance/retroceso de la carta
     *  @return La cantidad de casillas que avance/retrocede segun la especialidad del coco
     */
    public int defensa( int valor );
    
    /**
     *  Devuelve la descripcion de la especialidad.
     *  @return La descripcion de la especialidad
     */
    public String getDescripcion();
    
}   //fin de la interface Especialidad