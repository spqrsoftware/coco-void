package clases.especialidad;

public class PosibleMovimiento implements Especialidad{
    
    private String descripcion;
    
    public PosibleMovimiento() {
        descripcion = "70% de probabilidad de que se mueva";
    }
    
    /**
     *  Devuelve la descripcion de la especialidad.
     *  @return La descripcion de la especialidad
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *  Recibe un entero con el poder de la carta y devuelve
     *  el verdadero poder que realiza el coco.
     *  @param valor La cantidad de casillas de avance/retroceso de la carta
     *  @return La cantidad de casillas que avance/retrocede segun la especialidad del coco
     */
    public int defensa(int valor) {
        return valor;
    }

    /**
     *  Recibe un entero con la cantidad de casillas a moverse y devuelve
     *  el verdadero movimiento que realiza el coco.
     *  @param valor La cantidad de casillas de avance/retroceso de la carta
     *  @return La cantidad de casillas que avance/retrocede segun la especialidad del coco
     */
    public int mover(int valor) {
        int num = (int) Math.random();
        if (num <= 0.7){
            return valor;
        }
        return 0;
    }
    
}
