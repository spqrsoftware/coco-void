package clases.especialidad;

public class AvanzaDoble implements Especialidad{
    
    private String descripcion;
    
    public AvanzaDoble() {
        descripcion = "Avanza el doble";
    }

    /**
     *  Devuelve la descripcion de la especialidad.
     *  @return La descripcion de la especialidad
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *  Recibe un entero con el poder de la carta y devuelve
     *  el verdadero poder que realiza el coco.
     *  @param valor La cantidad de casillas de avance/retroceso de la carta
     *  @return La cantidad de casillas que avance/retrocede segun la especialidad del coco
     */
    public int defensa(int valor) {
        return valor;
    }

    /**
     *  Recibe un entero con la cantidad de casillas a moverse y devuelve
     *  el verdadero movimiento que realiza el coco.
     *  @param valor La cantidad de casillas de avance/retroceso de la carta
     *  @return La cantidad de casillas que avance/retrocede segun la especialidad del coco
     */
    public int mover(int valor) {
        return valor * 2;
    }
    
}   //fin de la clase AvanceDoble