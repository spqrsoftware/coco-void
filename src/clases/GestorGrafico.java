package clases;

import clases.otros.Coco;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

/**Usado para manejar cualquier custion grafica. En este momento solo se esta
 *encargando de los punteros que va a usar el juego. Es un singleton*/
public class GestorGrafico {
    
    private static GestorGrafico instancia;
    private Toolkit tk; //para manejar las cuestiones graficas
    private ImageIcon img;  //para acceder al archivo que tiene la imagen
    private Image puntero;  //para asignar la imagen al puntero
    
    
    private GestorGrafico() {
        tk = Toolkit.getDefaultToolkit();
    }   //fin del constructor
    
    /**Crea la instancia del GestorGrafico si no esta instanciado;
     *devuelve la instancia del GestorGrafico*/
    public static GestorGrafico getInstancia(){
        if( instancia == null ){
            instancia = new GestorGrafico();
            return instancia;
        }else{
            return instancia;
        }   //fin del if...else
    }   //fin del metodo getINstancia
    
    /**Cursor con forma de coco*/
    public Cursor coquismo(){
        img = new ImageIcon("src/imagenes/cursor.gif" );
        puntero = img.getImage();
        return tk.createCustomCursor(puntero, new Point(0,2), "Coco");
    }   //fin del metodo coquismo
    
    /**Cursor transparente, para dar la ilusion que no hay cursor. OJO. Usarce
     *con precaucion porque no se podra ver el cursor*/
    public Cursor sinCursor(){
        img = new ImageIcon("src/imagenes/No.gif" );
        puntero = img.getImage();
        return tk.createCustomCursor(puntero, new Point(0,0), "No");
    }   //fin del metodo coquismo
    
    /**Analiza al jugador actual, y devuelve la ruta relativa de la 
     * imagen del coco de medio lado de dicho jugador.
     *@returns: String*/
    public String imagenesCocosDerechoPeque(Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/CocoFreeRight.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco70'sRight.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/CocaRight.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/CocoMexRight.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/WarCocoRight.gif";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
    public String imagenesCocosDerechoGrandes(Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/CocoFreeR.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco70'sR.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/CocaR.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/CocoMexR.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/WarCocoR.jpg";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
    public String imagenesCocosIzquierdaPeque( Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/CocoFreeLeft.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco70'sLeft.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/CocaLeft.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/CocoMexLeft.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/WarCocoLeft.gif";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
    public String imagenesCocosIzquierdaGrandes( Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/CocoFreeL.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco70'sL.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/CocaL.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/CocoMexL.jpg";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/WarCocoL.jpg";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
    public String imagenesCocosFrente( Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/CocoFree.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco70's.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/Coca.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/CocoMex.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/WarCoco.gif";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
    public String imagenesCocosEspalda( Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/CocoFreeBack.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco70'sBack.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/CocaBack.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/CocoMexBack.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/WarCocoBack.gif";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
     /**Analiza al jugador actual, y devuelve la ruta relativa de la 
     * imagen del coco de medio lado de dicho jugador.
     *@returns: String*/
    public String retratosCocos(Coco c){
        if( c.getNombre().equals( "Coco Freeman") ){
            return "src/imagenes/cocos/Coco_FREE_static2.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco 70's") ){
            return "src/imagenes/cocos/Coco_70's.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco Bombeable") ){
            return "src/imagenes/cocos/Coca_Static.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "Coco") ){
            return "src/imagenes/cocos/Coco_MEX_Static.gif";
        }   //fin del if
        
        if( c.getNombre().equals( "WarCoco") ){
            return "src/imagenes/cocos/Coco_WAR_Static.gif";
        }   //fin del if
        return "";
    }   //fin del metodo initImagenes
    
    /*
     *Segun el valor del dado, devuelve la imagen correspondiente
     */
    public String analizarDado( int n ){
        switch( n ){
            case 1: return "src/imagenes/dados/uno.gif";
            case 2: return "src/imagenes/dados/dos.gif";
            case 3: return "src/imagenes/dados/tres.gif";
            case 4: return "src/imagenes/dados/cuatro.gif";
            case 5: return "src/imagenes/dados/cinco.gif";
            case 6: return "src/imagenes/dados/seis.gif";
            default: return "src/imagenes/dados/uno.gif";
        }   //fin del switch
    }   //fin del metodo analizarDado
    
}   //fin de la clase GestorGrafico