package clases;

import clases.Carta.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainCartas {
    
    public MainCartas() {
    }
    
    public static void main(String[] args) {
        String reverso = "src/imagenes/tarjetas/BackCard.jpg";
        String frenteAvance = "src/imagenes/tarjetas/GreenCard.jpg";
        String frenteDefensa = "src/imagenes/tarjetas/BlueCard.jpg";
        String frenteCastigo = "src/imagenes/tarjetas/BlackCard.jpg";
        String frenteRetroceso = "src/imagenes/tarjetas/RedCard.jpg";
        ArrayList arr;
        
        //Cartas de Avance (40)
        arr = new ArrayList();
        for (int i = 0; i < 40; i++) {
            arr.add(new CartaAvance(frenteAvance, reverso));
        }
        salvar(arr, "src/archivos/cartasAvance.coco");
        arr.clear();
        
        //Cartas de Retroceso (20)
        arr = new ArrayList();
        for (int i = 0; i < 20; i++) {
            arr.add(new CartaRetroceso(frenteRetroceso, reverso));
        }
        salvar(arr, "src/archivos/cartasRetroceso.coco");
        arr.clear();
        
        //Cartas de Castigo (15)
        arr = new ArrayList();
        for (int i = 0; i < 15; i++) {
            arr.add(new CartaCastigo(frenteCastigo, reverso));
        }
        salvar(arr, "src/archivos/cartasCastigo.coco");
        arr.clear();
        
        //Cartas de Defensa (15)
        arr = new ArrayList();
        for (int i = 0; i < 15; i++) {
            arr.add(new CartaDefensa(frenteDefensa, reverso));
        }
        salvar(arr, "src/archivos/cartasDefensa.coco");
        arr.clear();
    }
    
    private static void salvar(ArrayList ar, final String ARCHIVO){
        try{
            ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream(ARCHIVO));
            
            salida.writeObject(ar);
            salida.close();
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }
    
}
